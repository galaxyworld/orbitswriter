/*-------------------------------------------------
 *
 * OrbitsWriter - an Offline Blog Writer
 *
 * Copyright (C) 2012 devbean@galaxyworld.org
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 *-------------------------------------------------*/

#include <QDir>
#include <QFileInfoList>
#include <QMutex>
#include <QTime>

#include "plugincollection.h"
#include "pluginmanager.h"
#include "pluginmanager_p.h"
#include "pluginspec.h"
#include "pluginspec_p.h"
#include "common/global.h"

using namespace PluginSystem;
using namespace PluginSystem::Internal;

PluginManager * PluginManager::m_instance = 0;

static bool lessThanByPluginName(const PluginSpec *one, const PluginSpec *two)
{
    return one->name() < two->name();
}

// ========== PluginManager ==========

PluginManager * PluginManager::instance()
{
    GET_INSTANCE(m_instance, PluginManager);
}

PluginManager::PluginManager() :
    d(new Internal::PluginManagerPrivate(this))
{
}

PluginManager::~PluginManager()
{
    delete d;
    d = 0;
}

void PluginManager::loadPlugins() const
{
    d->loadPlugins();
}

void PluginManager::unloadPlugins() const
{
    d->unloadPlugins();
}

QList<PluginSpec *> PluginManager::plugins() const
{
    return d->pluginSpecs;
}

bool PluginManager::hasError() const
{
    foreach (PluginSpec *spec, plugins()) {
        // only show errors on startup if plugin is enabled.
        if (spec->hasError() && spec->isEnabled() && !spec->isDisabledIndirectly()) {
            return true;
        }
    }
    return false;
}

QHash<QString, PluginCollection *> PluginManager::pluginCollections() const
{
    return d->pluginCategories;
}

void PluginManager::setPluginPaths(const QStringList &paths)
{
    d->setPluginPaths(paths);
}

// ========== PluginSystem::Internal::PluginManagerPrivate ==========

/*!
  \class PluginSystem::Internal::PluginManagerPrivate
  \brief PluginManagerPrivate
 */

PluginManagerPrivate::PluginManagerPrivate(PluginManager *pluginManager) :
    extension(QLatin1String("spec")),
    q(pluginManager)
{
}

PluginManagerPrivate::~PluginManagerPrivate()
{
    qDeleteAll(pluginSpecs);
    qDeleteAll(pluginCategories);
}

void PluginManagerPrivate::loadPlugins()
{
    QList<PluginSpec *> queue = loadQueue();
    foreach (PluginSpec *spec, queue) {
        loadPlugin(spec, PluginSpec::Loaded);
    }
    foreach (PluginSpec *spec, queue) {
        loadPlugin(spec, PluginSpec::Initialized);
    }
    QListIterator<PluginSpec *> it(queue);
    it.toBack();
    while (it.hasPrevious()) {
        PluginSpec *spec = it.previous();
        loadPlugin(spec, PluginSpec::Running);
        if (spec->state() == PluginSpec::Running) {
            delayedInitializeQueue.append(spec);
        }
    }
    emit q->pluginsChanged();

//    delayedInitializeTimer = new QTimer;
//    delayedInitializeTimer->setInterval(DELAYED_INITIALIZE_INTERVAL);
//    delayedInitializeTimer->setSingleShot(true);
//    connect(delayedInitializeTimer, SIGNAL(timeout()),
//            this, SLOT(nextDelayedInitialize()));
    //    delayedInitializeTimer->start();
}

void PluginManagerPrivate::unloadPlugins()
{
    stopAll();
//    if (!asynchronousPlugins.isEmpty()) {
//        shutdownEventLoop = new QEventLoop;
//        shutdownEventLoop->exec();
//    }
    deleteAll();
//    if (!allObjects.isEmpty()) {
//        qDebug() << "There are" << allObjects.size() << "objects left in the plugin manager pool: " << allObjects;
//    }
}

void PluginManagerPrivate::stopAll()
{
//    if (delayedInitializeTimer && delayedInitializeTimer->isActive()) {
//        delayedInitializeTimer->stop();
//        delete delayedInitializeTimer;
//        delayedInitializeTimer = 0;
//    }
    QList<PluginSpec *> queue = loadQueue();
    foreach (PluginSpec *spec, queue) {
        loadPlugin(spec, PluginSpec::Stopped);
    }
}

void PluginManagerPrivate::deleteAll()
{
    QList<PluginSpec *> queue = loadQueue();
    QListIterator<PluginSpec *> it(queue);
    it.toBack();
    while (it.hasPrevious()) {
        loadPlugin(it.previous(), PluginSpec::Deleted);
    }
}

void PluginManagerPrivate::readPluginPaths()
{
    qDeleteAll(pluginCategories);
    qDeleteAll(pluginSpecs);
    pluginSpecs.clear();
    pluginCategories.clear();

    QStringList specFiles;
    QStringList searchPaths = pluginPaths;
    while (!searchPaths.isEmpty()) {
        const QDir dir(searchPaths.takeFirst());
        const QString pattern = QLatin1String("*.") + extension;
        const QFileInfoList files = dir.entryInfoList(QStringList(pattern), QDir::Files);
        foreach (const QFileInfo &file, files) {
            specFiles << file.absoluteFilePath();
        }
        const QFileInfoList dirs = dir.entryInfoList(QDir::Dirs|QDir::NoDotAndDotDot);
        foreach (const QFileInfo &subdir, dirs) {
            searchPaths << subdir.absoluteFilePath();
        }
    }
    defaultCollection = new PluginCollection(QString());
    pluginCategories.insert("", defaultCollection);

    foreach (const QString &specFile, specFiles) {
        PluginSpec *spec = new PluginSpec;
        spec->d->read(specFile);

        PluginCollection *collection = 0;
        // find correct plugin collection or create a new one
        if (pluginCategories.contains(spec->category())) {
            collection = pluginCategories.value(spec->category());
        } else {
            collection = new PluginCollection(spec->category());
            pluginCategories.insert(spec->category(), collection);
        }
        if (defaultDisabledPlugins.contains(spec->name())) {
            spec->setDisabledByDefault(true);
            spec->setEnabled(false);
        }
        if (spec->isDisabledByDefault() && forceEnabledPlugins.contains(spec->name())) {
            spec->setEnabled(true);
        }
        if (!spec->isDisabledByDefault() && disabledPlugins.contains(spec->name())) {
            spec->setEnabled(false);
        }

        collection->addPlugin(spec);
        pluginSpecs.append(spec);
    }
    resolveDependencies();
    // ensure deterministic plugin load order by sorting
    qSort(pluginSpecs.begin(), pluginSpecs.end(), lessThanByPluginName);
    emit q->pluginsChanged();
}

void PluginManagerPrivate::resolveDependencies()
{
    foreach (PluginSpec *spec, pluginSpecs) {
        spec->d->resolveDependencies(pluginSpecs);
    }
    foreach (PluginSpec *spec, loadQueue()) {
        spec->d->disableIndirectlyIfDependencyDisabled();
    }
}

QList<PluginSpec *> PluginManagerPrivate::loadQueue()
{
    QList<PluginSpec *> queue;
    foreach (PluginSpec *spec, pluginSpecs) {
        QList<PluginSpec *> circularityCheckQueue;
        loadQueue(spec, queue, circularityCheckQueue);
    }
    return queue;
}

bool PluginManagerPrivate::loadQueue(PluginSpec *spec,
                                     QList<PluginSpec *> &queue,
                                     QList<PluginSpec *> &circularityCheckQueue)
{
    if (queue.contains(spec)) {
        return true;
    }
    // check for circular dependencies
    if (circularityCheckQueue.contains(spec)) {
        spec->d->hasError = true;
        spec->d->errorString = PluginManager::tr("Circular dependency detected:\n");
        int index = circularityCheckQueue.indexOf(spec);
        for (int i = index; i < circularityCheckQueue.size(); ++i) {
            spec->d->errorString.append(PluginManager::tr("%1(%2) depends on\n")
                .arg(circularityCheckQueue.at(i)->name()).arg(circularityCheckQueue.at(i)->version()));
        }
        spec->d->errorString.append(PluginManager::tr("%1(%2)").arg(spec->name()).arg(spec->version()));
        return false;
    }
    circularityCheckQueue.append(spec);
    // check if we have the dependencies
    if (spec->state() == PluginSpec::Invalid || spec->state() == PluginSpec::Read) {
        queue.append(spec);
        return false;
    }

    // add dependencies
    foreach (PluginSpec *depSpec, spec->dependencySpecs()) {
        if (!loadQueue(depSpec, queue, circularityCheckQueue)) {
            spec->d->hasError = true;
            spec->d->errorString = PluginManager::tr("Cannot load plugin because dependency failed to load: %1(%2)\nReason: %3")
                    .arg(depSpec->name()).arg(depSpec->version()).arg(depSpec->errorString());
            return false;
        }
    }
    // add self
    queue.append(spec);
    return true;
}

void PluginManagerPrivate::loadPlugin(PluginSpec *spec, PluginSpec::State destState)
{
    if (spec->hasError() || spec->state() != destState - 1) {
        return;
    }

    // don't load disabled plugins.
    if ((spec->isDisabledIndirectly() || !spec->isEnabled()) && destState == PluginSpec::Loaded) {
        return;
    }

    switch (destState) {
    case PluginSpec::Running:
        profilingReport(">initializeExtensions", spec);
        spec->d->initializePlugins();
        profilingReport("<initializeExtensions", spec);
        return;
    case PluginSpec::Deleted:
        profilingReport(">delete", spec);
        spec->d->kill();
        profilingReport("<delete", spec);
        return;
    default:
        break;
    }
    // check if dependencies have loaded without error
    QHashIterator<PluginDependency, PluginSpec *> it(spec->dependencySpecs());
    while (it.hasNext()) {
        it.next();
        if (it.key().type == PluginDependency::Optional)
            continue;
        PluginSpec *depSpec = it.value();
        if (depSpec->state() != destState) {
            spec->d->hasError = true;
            spec->d->errorString = PluginManager::tr("Cannot load plugin because dependency failed to load: %1(%2)\nReason: %3")
                    .arg(depSpec->name()).arg(depSpec->version()).arg(depSpec->errorString());
            return;
        }
    }
    switch (destState) {
    case PluginSpec::Loaded:
        profilingReport(">loadLibrary", spec);
        spec->d->load();
        profilingReport("<loadLibrary", spec);
        break;
    case PluginSpec::Initialized:
        profilingReport(">initializePlugin", spec);
        spec->d->initializePlugin();
        profilingReport("<initializePlugin", spec);
        break;
    case PluginSpec::Stopped:
        profilingReport(">stop", spec);
//        if (spec->d->stop() == IPlugin::AsynchronousShutdown) {
//            asynchronousPlugins << spec;
//            connect(spec->plugin(), SIGNAL(asynchronousShutdownFinished()),
//                    this, SLOT(asyncShutdownFinished()));
//        }
        profilingReport("<stop", spec);
        break;
    default:
        break;
    }
}

PluginSpec *PluginManagerPrivate::pluginByName(const QString &name) const
{
    foreach (PluginSpec *spec, pluginSpecs) {
        if (spec->name() == name) {
            return spec;
        }
    }
    return 0;
}

void PluginManagerPrivate::setPluginPaths(const QStringList &paths)
{
    pluginPaths = paths;
//    readSettings();
    readPluginPaths();
}

void PluginManagerPrivate::initProfiling()
{
    if (profileTimer.isNull()) {
        profileTimer.reset(new QTime);
        profileTimer->start();
        profileElapsedMS = 0;
        qDebug("Profiling started");
    } else {
        profilingVerbosity++;
    }
}

void PluginManagerPrivate::profilingReport(const char *what, const PluginSpec *spec)
{
    if (!profileTimer.isNull()) {
        const int absoluteElapsedMS = profileTimer->elapsed();
        const int elapsedMS = absoluteElapsedMS - profileElapsedMS;
        profileElapsedMS = absoluteElapsedMS;
        if (spec) {
            qDebug("%-22s %-22s %8dms (%8dms)", what, qPrintable(spec->name()), absoluteElapsedMS, elapsedMS);
        } else {
            qDebug("%-45s %8dms (%8dms)", what, absoluteElapsedMS, elapsedMS);
        }
    }
}
