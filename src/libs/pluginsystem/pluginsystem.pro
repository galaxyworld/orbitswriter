TEMPLATE = lib
TARGET   = PluginSystem
DEFINES += PLUGINSYSTEM_LIBRARY

include(../../library.pri)
include(pluginsystem_dependencies.pri)

unix:!macx:!freebsd*:LIBS += -ldl

!isEmpty(vcproj) {
    DEFINES += APP_TEST_DIR=\"$$APP_SOURCE_TREE\"
} else {
    DEFINES += APP_TEST_DIR=\\\"$$APP_SOURCE_TREE\\\"
}

HEADERS += \
    pluginmanager.h \
    pluginspec.h \
    pluginspec_p.h \
    pluginmanager_p.h \
    plugin.h \
    plugin_p.h \
    plugincollection.h \
    pluginsystem_global.h

SOURCES += \
    pluginmanager.cpp \
    pluginspec.cpp \
    plugin.cpp \
    plugincollection.cpp
