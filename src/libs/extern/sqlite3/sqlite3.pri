CONFIG(release, debug|release):DEFINES *= NDEBUG

DEFINES += SQLITE_HAS_CODEC

INCLUDEPATH += $$PWD

DEPENDPATH  += $$PWD
 
HEADERS  += \
    codec.h \
    rijndael.h \
    sqlite3.h \
    sqlite3ext.h

SOURCES  += \
    codec.c \
    codecext.c \
    rijndael.c \
    shell.c \
    sqlite3.c \
    sqlite3secure.c
