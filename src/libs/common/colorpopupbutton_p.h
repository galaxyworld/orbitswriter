/*-------------------------------------------------
 *
 * OrbitsWriter - an Offline Blog Writer
 *
 * Copyright (C) 2012 devbean@galaxyworld.org
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 *-------------------------------------------------*/

#ifndef COLORPOPUPBUTTON_P_H
#define COLORPOPUPBUTTON_P_H

#include <QColor>
#include <QObject>
#include <QPixmap>

class QAbstractButton;
class QIcon;
class QModelIndex;
class QSize;

namespace Common
{

class ColorModel;

namespace Internal
{

class ColorPopup;

class ColorPopupButtonPrivate : public QObject
{
    Q_OBJECT
public:
    ColorPopupButtonPrivate(QAbstractButton *btn);

    QSize popupSizeHint() const;
    QList<QColor> colors() const;
    void setStandardColors();
    void setCurrentColor(const QColor &color);
    void updateIcon();

    ColorPopup *popup;
    ColorModel *colorModel;
    QColor      currentColor;
    QString     currentColorName;
    QPixmap     iconPixmap;

public slots:
    void showPopup();
    void setCurrentIndex(const QModelIndex &index);
    void showColorDialog();

signals:
    void currentColorChanged(const QColor &color);

private:
    QAbstractButton *q;
}; // end of class Common::Internal::ColorPopupButtonPrivate

} // end of namespace Common::Internal
} // end of namespace Common

#endif // COLORPOPUPBUTTON_P_H
