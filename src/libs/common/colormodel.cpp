/*-------------------------------------------------
 *
 * OrbitsWriter - an Offline Blog Writer
 *
 * Copyright (C) 2012 devbean@galaxyworld.org
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 *-------------------------------------------------*/

#include "colormodel.h"

using namespace Common;

ColorModel::ColorModel(QObject *parent) :
    QStandardItemModel(parent)
{
    setColumnCount(1);
}

QModelIndex ColorModel::contains(const QColor &color)
{
    QVariant var(color);
    QModelIndexList lst = match(index(0, 0), Qt::DecorationRole, var, 1, Qt::MatchExactly);
    return lst.isEmpty() ? QModelIndex() : lst.first();
}

QModelIndex ColorModel::appendColor(const QColor &color, const QString &name)
{
    return insertColor(rowCount(), color, name);
}

QModelIndex ColorModel::insertColor(int index, const QColor &color, const QString &name)
{
    QStandardItem *item = new QStandardItem;
    item->setText(name);
    item->setData(color, Qt::DecorationRole);
    item->setData(name, Qt::ToolTipRole);
    insertRow(index, item);
    return item->index();
}
