/*-------------------------------------------------
 *
 * OrbitsWriter - an Offline Blog Writer
 *
 * Copyright (C) 2012 devbean@galaxyworld.org
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 *-------------------------------------------------*/

#include <QtGlobal>
#include "loggingtarget.h"

using namespace Common;

// ========== Common::FileLoggingTarget ==========

void FileLoggingTarget::write(const QString &message)
{
    m_out << message << endl;
    m_out.flush();
}

void FileLoggingTarget::setPath(const QString &path, QString *err)
{
    m_file.setFileName(path);
    if (!m_file.open(QFile::WriteOnly | QFile::Text | QFile::Append)) {
        *err = m_file.errorString();
        return;
    } else {
        *err = QString();
    }
    m_out.setDevice(&m_file);
}


// ========== Common::ConsoleLoggingTarget ==========

#if defined(Q_OS_WIN)
#  define WIN32_LEAN_AND_MEAN
#  include <Windows.h>
void ConsoleLoggingTarget::write(const QString &message)
{
   OutputDebugStringW(reinterpret_cast<const WCHAR*>(message.utf16()));
   OutputDebugStringW(L"\n");
}
#elif defined(Q_OS_SYMBIAN)
#  include <e32debug.h>
void ConsoleLoggingTarget::write(const QString &message)
{
   TPtrC8 symbianMessage(reinterpret_cast<const TUint8*>(qPrintable(message)));
   RDebug::RawPrint(symbianMessage);
}
#elif defined(Q_OS_UNIX)
#  include <cstdio>
void ConsoleLoggingTarget::write(const QString &message)
{
   fprintf(stderr, "%s\n", qPrintable(message));
   fflush(stderr);
}
#endif
