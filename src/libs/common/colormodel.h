/*-------------------------------------------------
 *
 * OrbitsWriter - an Offline Blog Writer
 *
 * Copyright (C) 2012 devbean@galaxyworld.org
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 *-------------------------------------------------*/

#ifndef COLORMODEL_H
#define COLORMODEL_H

#include <QStandardItemModel>

#include "common_global.h"

namespace Common
{

/*!
  The model for colors which could be used for Model/View framework.
 */
class COMMON_EXPORT ColorModel : public QStandardItemModel
{
    Q_OBJECT
public:
    /*!
      Constructs an instance of ColorModel.
     */
    ColorModel(QObject *parent = 0);

    /*!
      Returns the model index of given \a color in this model.

      If there is no such \a color, an invalid index will be returned.
     */
    QModelIndex contains(const QColor &color);

    /*!
      Appends a new \a color with \a name to the model.
     */
    QModelIndex appendColor(const QColor &color, const QString &name = QString::null);

    /*!
      Inserts a new \a color with \a name at \a index of the model.
     */
    QModelIndex insertColor(int index, const QColor &color, const QString &name = QString::null);
}; // end of class Common::ColorModel

} // end of namespace Common

#endif // COLORMODEL_H
