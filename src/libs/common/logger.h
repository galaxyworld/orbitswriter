/*-------------------------------------------------
 *
 * OrbitsWriter - an Offline Blog Writer
 *
 * Copyright (C) 2012 devbean@galaxyworld.org
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 *-------------------------------------------------*/

#ifndef LOGGER_H
#define LOGGER_H

#include "common_global.h"
#include "loggingtarget.h"

// ---------- log ---------- //
//! This macro expands log an trace message \a message.
#define LOG_TRACE(message) Common::Logger::instance()->trace(message);
//! This macro expands log an debug message \a message.
#define LOG_DEBUG(message) Common::Logger::instance()->debug(message);
//! This macro expands log an info message \a message.
#define LOG_INFO(message) Common::Logger::instance()->info(message);
//! This macro expands log a warning message \a message.
#define LOG_WARN(message) Common::Logger::instance()->warn(message);
//! This macro expands log an error message \a message.
#define LOG_ERROR(message) Common::Logger::instance()->error(message);
//! This macro expands log a fatal message \a message.
#define LOG_FATAL(message) Common::Logger::instance()->fatal(message);

class QString;

namespace Common
{

namespace Internal
{
class LoggerPrivate;
} // end of namespace Common::Internal

//! This enum describes the logging level for application logger.
enum LoggingLevel
{
   Trace = 0, //!< Trace level.
   Debug,     //!< Debug level.
   Info,      //!< Info level.
   Warn,      //!< Warn level.
   Error,     //!< Error level.
   Fatal      //!< Fatal level.
};

/*!
  The logger is an application logger used for core and plugins.

  By default, we choose logs of priority info and above. The log file will be created
  at the same directory as the executable.

  There are six logging levels provided by logger:
  - Trace
  - Debug
  - Info
  - Warn
  - Error
  - Fatal
  You could set the level using Common::LoggingLevel enum. Logging level is global and this is
  set at the beginning of the application. If you change the logging level anywhere else,
  this will effect in application scope.

  To log messages, you could use trace(), debug(), info(), warn(), error() and fatal() methods.
  But macros such as LOG_TRACE(), LOG_DEBUG(), LOG_INFO(), LOG_WARN(),
  LOG_ERROR() and LOG_FATAL() are more recommended.

  This is a singleton class. If you need access to its instance, use
  Logger::instance() method.
 */
class COMMON_EXPORT Logger
{
public:
    /*!
      Gets the singleton instance of Logger.
     */
    static Logger * instance();

    // ---------- settings ---------- //
    //! Sets application logging level.
    void setLogLevel(LoggingLevel level);

    //! Returns the application loggin level.
    LoggingLevel logLevel() const;

    /*!
      Adds logging \a target to logger.

      Logger will not manage the memory about \a target. You should release these
      target instances by yourself.
     */
    void addLoggingTarget(LoggingTarget * target);

    // ---------- logging methods ---------- //
    //! Logs trace message to all register logging targets.
    void trace(const QString & message) const;
    //! Logs debug message to all register logging targets.
    void debug(const QString & message) const;
    //! Logs information message to all register logging targets.
    void info(const QString & message) const;
    //! Logs warn message to all register logging targets.
    void warn(const QString & message) const;
    //! Logs error message to all register logging targets.
    void error(const QString & message) const;
    //! Logs fatal message to all register logging targets.
    void fatal(const QString & message) const;

private:
    //! Constructs an instance of Logger.
    Logger();
    //! Destroys the instance of logger.
    ~Logger();
    Q_DISABLE_COPY(Logger)

    static Logger *m_instance;

    Internal::LoggerPrivate *d;
    friend class Internal::LoggerPrivate;
}; // end of class Common::Logger

} // end of namespace Common

#endif // LOGGER_H
