/*-------------------------------------------------
 *
 * OrbitsWriter - an Offline Blog Writer
 *
 * Copyright (C) 2012 devbean@galaxyworld.org
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 *-------------------------------------------------*/

#ifndef COLORTOOLBUTTON_H
#define COLORTOOLBUTTON_H

#include <QPushButton>
#include <QToolButton>

#include "common_global.h"

template <typename T> class QList;
class QModelIndex;

namespace Common
{

namespace Internal
{
class ColorPopupButtonPrivate;
}

/*!
  The ColorToolButton class is a button which could show a color popup
  which is usually used in tool bars.

  The color popup will add all standard colors by default. At the bottom of
  color popup there is a button which could show a color dialog.

  When current color changed, currentColorChanged(QColor) will be emitted.
 */
class COMMON_EXPORT ColorToolButton : public QToolButton
{
    Q_OBJECT
public:
    /*!
      Constructs an instance of ColorToolButton with given \a parent.
     */
    explicit ColorToolButton(QWidget *parent = 0);

    /*!
      Destructs the instance of ColorToolButton.
     */
    ~ColorToolButton();

    /*!
      Adds a new \a color with \a name to the button popup.
     */
    void addColor(const QColor &color, const QString &name = QString::null);

    /*!
      Clears the underlying color model.
     */
    void clear();

    /*!
      Returns all colors in this color model.
     */
    QList<QColor> colors() const;

    /*!
      Sets standard colors for this button.
     */
    void setStandardColors();

    /*!
      Returns current color of this popup button.
     */
    QColor currentColor() const;
//    bool dragEnabled() const;
//    void setColors(const QStringList &);
//    void setDragEnabled(bool v);
//    bool showName() const;

signals:
    void currentColorChanged(const QColor &color);

public slots:
    /*!
      Sets current color to \a color.

      If the color is already in color model, the color will be selected;
      otherwise, a new color will be added to the model with a name
      formatted in #RRGGBB.
     */
    void setCurrentColor(const QColor &color);

private:
    Internal::ColorPopupButtonPrivate *d;
    friend class Internal::ColorPopupButtonPrivate;
};

// end of class Common::ColorPopupButton

} // end of namespace Common

#endif // COLORTOOLBUTTON_H
