/*-------------------------------------------------
 *
 * OrbitsWriter - an Offline Blog Writer
 *
 * Copyright (C) 2012 devbean@galaxyworld.org
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 *-------------------------------------------------*/

#ifndef LOGGINGTARGET_H
#define LOGGINGTARGET_H

#include <QFile>
#include <QTextStream>

#include "common_global.h"

namespace Common
{

/*!
  The logging system target interface.

  Application logger will accept this target to log information.
 */
class COMMON_EXPORT LoggingTarget
{
public:
    //! Destorys the instance of LoggingTarget.
    virtual ~LoggingTarget() {}

    //! Writes \a message to this target.
    virtual void write(const QString & message) = 0;
}; // end of class Common::LoggingTarget

/*!
  The logging target to local file.
 */
class COMMON_EXPORT FileLoggingTarget : public LoggingTarget
{
public:
    // LoggingTarget {
    void write(const QString & message);
    // }

    /*!
      Sets target \a path to log.

      This is useful for target which should log to file.
      If anything wrong while setting, \a err should fill with the error message,
      otherwise \a err will be empty.
     */
    void setPath(const QString & path, QString * err);

private:
    QFile m_file;
    QTextStream m_out;
}; // end of class Common::FileLoggingTarget

/*!
  The logging target to console.
 */
class COMMON_EXPORT ConsoleLoggingTarget : public LoggingTarget
{
public:
    // LoggingTarget {
    void write(const QString & message);
    // }
}; // end of class Common::FileLoggingTarget

} // end of namespace Common

#endif // LOGGINGTARGET_H
