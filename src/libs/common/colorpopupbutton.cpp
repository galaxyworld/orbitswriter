/*-------------------------------------------------
 *
 * OrbitsWriter - an Offline Blog Writer
 *
 * Copyright (C) 2012 devbean@galaxyworld.org
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 *-------------------------------------------------*/

#include <QAbstractItemDelegate>
#include <QColorDialog>
#include <QDebug>
#include <QDesktopWidget>
#include <QListView>
#include <QPainter>
#include <QVBoxLayout>

#include "colormodel.h"
#include "colorbutton.h"
#include "colorpopupbutton_p.h"
#include "colortoolbutton.h"

static const int COLORBLOCK_SIZE = 16;
static const int OFFSET_POINT_SIZE = 4;
static const QPoint OFFSET_POINT(OFFSET_POINT_SIZE, OFFSET_POINT_SIZE);

namespace Common
{

namespace Internal
{

// ========== Core::Internal::ColorDelegate ==========

class ColorDelegate : public QAbstractItemDelegate
{
public:
    ColorDelegate(QObject *parent = 0) :
        QAbstractItemDelegate(parent)
    {
    }

    void paint(QPainter *painter, const QStyleOptionViewItem &option, const QModelIndex &index) const
    {
        if (!index.isValid()) {
            return;
        }
        QColor color = index.data(Qt::DecorationRole).value<QColor>();
        painter->save();
        painter->setBrush(color);
        painter->drawRect(option.rect);
        if (option.state & QStyle::State_Selected) {
            QPen pen = painter->pen();
            pen.setColor(QColor::fromRgb(255 - color.red(),
                                         255 - color.green(),
                                         255 - color.blue()));
            pen.setWidth(2);
            painter->setPen(pen);
//            painter->drawRect(option.rect.adjusted(1, 1, 0, 0));
            QPoint topLeft = option.rect.topLeft();
            QPoint topRight = option.rect.topRight();
            QPoint bottomLeft = option.rect.bottomLeft();
            QPoint bottomRight = option.rect.bottomRight();
            painter->drawLine(topLeft + OFFSET_POINT, bottomRight - OFFSET_POINT);
            painter->drawLine(QPoint(topRight.x() - OFFSET_POINT_SIZE, topRight.y() + OFFSET_POINT_SIZE),
                              QPoint(bottomLeft.x() + OFFSET_POINT_SIZE, bottomLeft.y() - OFFSET_POINT_SIZE));
        }
        painter->restore();
    }

    QSize sizeHint(const QStyleOptionViewItem &option, const QModelIndex &index) const
    {
        Q_UNUSED(option);
        Q_UNUSED(index);
        return QSize(COLORBLOCK_SIZE, COLORBLOCK_SIZE);
    }
}; // end of class Common::Internal::ColorDelegate


// ========== Core::Internal::ColorPopup ==========

class ColorPopup : public QWidget
{
public:
    ColorPopup(ColorModel *model, QWidget *parent = 0) :
        QWidget(parent, Qt::Popup),
        colorView(new QListView(this)),
        moreColorButton(new QPushButton(this))
    {
        colorView->setFlow(QListView::LeftToRight);
        colorView->setResizeMode(QListView::Adjust);
        colorView->setWrapping(true);
        colorView->setUniformItemSizes(true);
        colorView->setMouseTracking(true);
        colorView->setHorizontalScrollBarPolicy(Qt::ScrollBarAlwaysOff);
        colorView->setItemDelegate(new ColorDelegate(this));
        colorView->setModel(model);

        moreColorButton->setText(tr("More..."));
        moreColorButton->setSizePolicy(QSizePolicy::Expanding, QSizePolicy::Fixed);

        QVBoxLayout *layout = new QVBoxLayout(this);
        layout->setSpacing(0);
        layout->setMargin(0);
        layout->addWidget(colorView);
        layout->addWidget(moreColorButton);
        setMaximumHeight(250);
    }

    QListView   *colorView;
    QPushButton *moreColorButton;
}; // end of class Common::Internal::ColorPopup


// ========== Core::Internal::ColorPopupButtonPrivate ==========

ColorPopupButtonPrivate::ColorPopupButtonPrivate(QAbstractButton *btn) :
    popup(0),
    q(btn)
{
}

void ColorPopupButtonPrivate::showPopup()
{
    if (!popup) {
        popup = new ColorPopup(colorModel, q);
        connect(popup->colorView, SIGNAL(clicked(QModelIndex)), this, SLOT(setCurrentIndex(QModelIndex)));
        connect(popup->moreColorButton, SIGNAL(clicked()), this, SLOT(showColorDialog()));
    }
    QPoint pos = q->rect().bottomLeft();
    pos = q->mapToGlobal(pos);
    QRect avail = QDesktopWidget().availableGeometry();
    int height = avail.height() - pos.y();
    popup->move(pos);
    QSize hint = popupSizeHint();
    if(hint.height() > height) {
        hint.setHeight(height);
    }
    popup->setFixedSize(hint);
    popup->show();
}

QSize ColorPopupButtonPrivate::popupSizeHint() const
{
    if (!colorModel) {
        return QSize(80, 100);
    }

    const int redefinedColumnCount = 8;
    const int margin = (q->style()->pixelMetric(QStyle::PM_DefaultFrameWidth, 0, q) << 1) + 1;
    const int colorCount = colorModel->rowCount();
    int columnCount = qMax(redefinedColumnCount, (q->width() - margin) / COLORBLOCK_SIZE);
    int rowCount = colorCount % columnCount == 0 ? colorCount / columnCount : (1 + colorCount / columnCount);
    int scrollBarExtent = q->style()->pixelMetric(QStyle::PM_ScrollBarExtent, 0, q);
    if(rowCount <= 12) {
        return QSize(columnCount * COLORBLOCK_SIZE + margin, rowCount * COLORBLOCK_SIZE + margin + 27);
    } else {
        columnCount = qMax(redefinedColumnCount, (q->width() - margin - scrollBarExtent) / COLORBLOCK_SIZE);
        rowCount = colorCount / columnCount;
        return QSize(columnCount * COLORBLOCK_SIZE + margin + 1, COLORBLOCK_SIZE * 12 + margin + 30);
    }
}

QList<QColor> ColorPopupButtonPrivate::colors() const
{
    QList<QColor> colorList;
    for (int i = 0; i < colorModel->rowCount(); i++) {
        QModelIndex index = colorModel->index(i, 0);
        colorList.append(index.data(Qt::DecorationRole).value<QColor>());
    }
    return colorList;
}

void ColorPopupButtonPrivate::setStandardColors()
{
    QStringList colorList = QColor::colorNames();
    colorModel->clear();
    foreach(QString colorName, colorList) {
        QColor color;
        color.setNamedColor(colorName);
        colorModel->appendColor(color, colorName);
    }
}

void ColorPopupButtonPrivate::setCurrentIndex(const QModelIndex &index)
{
    if (popup) {
        popup->hide();
    }
    QColor color = index.data(Qt::DecorationRole).value<QColor>();
    currentColor = color;
    currentColorName = index.data(Qt::ToolTipRole).toString();
    updateIcon();
    if(color.isValid()) {
        q->setToolTip(currentColorName);
        emit currentColorChanged(color);
    }
    if(popup) {
        popup->colorView->setCurrentIndex(index);
    }
}

void ColorPopupButtonPrivate::showColorDialog()
{
    QColor color = QColorDialog::getColor(currentColor, q, tr("Choose Color"), QColorDialog::ShowAlphaChannel);
    if (color.isValid()) {
        setCurrentColor(color);
    }
}

/*!
  Gets the name of \a color.

  If the color has a standard name in Qt, the name will be returned,
  otherwise return an empty string.
 */
static QString colorName(const QColor &color)
{
    foreach (QString name, QColor::colorNames()) {
        QColor c;
        c.setNamedColor(name);
        if (color == c) {
            return name;
        }
    }
    return QString();
}

void ColorPopupButtonPrivate::setCurrentColor(const QColor &color)
{
    if (color == currentColor) {
        return;
    }
    QModelIndex index = colorModel->contains(color);
    if (!index.isValid()) {
        QString stdName = colorName(color);
        colorModel->appendColor(color, stdName.isEmpty() ? color.name() : stdName);
        index = colorModel->index(colorModel->rowCount() - 1, 0);
    }
    setCurrentIndex(index);
}

void ColorPopupButtonPrivate::updateIcon()
{
    const int pixSize = 18;
    const int icoSize = 30;
    const int colorBlockWidth = 24;
    const int colorBlockHeight = 14;
    if (!q->icon().isNull() && iconPixmap.isNull()) {
        iconPixmap = QPixmap(q->icon().pixmap(QSize(pixSize, pixSize)));
    }

    QPixmap px(icoSize, icoSize);
    px.fill(Qt::transparent);
    QPainter painter(&px);
    if (q->icon().isNull()) {
        painter.fillRect(4, 4, px.width() - 1, px.height() - 1, currentColor);
    } else {
        painter.drawPixmap((icoSize - pixSize) >> 1, 1, iconPixmap);
        painter.fillRect((icoSize - colorBlockWidth) >> 1, icoSize - 10,
                         colorBlockWidth, colorBlockHeight,
                         currentColor);
    }

    q->setIcon(QIcon(px));
}

} // end of namespace Common::Internal


// ========== Core::ColorButton ==========

ColorButton::ColorButton(QWidget *parent) :
    QPushButton(parent),
    d(new Internal::ColorPopupButtonPrivate(this))
{
    d->colorModel = new ColorModel(this);

    setStandardColors();
    connect(this, SIGNAL(clicked()), d, SLOT(showPopup()));
    connect(d, SIGNAL(currentColorChanged(QColor)), this, SIGNAL(currentColorChanged(QColor)));
}

ColorButton::~ColorButton()
{
    delete d;
}

void ColorButton::addColor(const QColor &color, const QString &name)
{
    d->colorModel->appendColor(color, name);
}

void ColorButton::clear()
{
    d->colorModel->clear();
}

QList<QColor> ColorButton::colors() const
{
    return d->colors();
}

void ColorButton::setCurrentColor(const QColor &color)
{
    d->setCurrentColor(color);
}

void ColorButton::setStandardColors()
{
    d->setStandardColors();
}

QColor ColorButton::currentColor() const
{
    return d->currentColor;
}


// ========== Core::ColorToolButton ==========

ColorToolButton::ColorToolButton(QWidget *parent) :
    QToolButton(parent),
    d(new Internal::ColorPopupButtonPrivate(this))
{
    d->colorModel = new ColorModel(this);

    setStandardColors();
    connect(this, SIGNAL(clicked()), d, SLOT(showPopup()));
    connect(d, SIGNAL(currentColorChanged(QColor)), this, SIGNAL(currentColorChanged(QColor)));
}

ColorToolButton::~ColorToolButton()
{
    delete d;
}

void ColorToolButton::addColor(const QColor &color, const QString &name)
{
    d->colorModel->appendColor(color, name);
}

void ColorToolButton::clear()
{
    d->colorModel->clear();
}

QList<QColor> ColorToolButton::colors() const
{
    return d->colors();
}

void ColorToolButton::setStandardColors()
{
    d->setStandardColors();
}

QColor ColorToolButton::currentColor() const
{
    return d->currentColor;
}

void ColorToolButton::setCurrentColor(const QColor &color)
{
    d->setCurrentColor(color);
}

} // end of namespace Common
