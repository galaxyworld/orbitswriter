TEMPLATE = lib
TARGET   = Common
DEFINES += COMMON_LIBRARY

include(../../library.pri)

HEADERS += \
    aggregate.h \
    common_global.h \
    colormodel.h\
    global.h \
    logger.h \
    loggingtarget.h \
    objectpool.h \
    logger.h \
    settingsmanager.h \
    colorpopupbutton_p.h \
    colorbutton.h \
    colortoolbutton.h

SOURCES += \
    aggregate.cpp \
    colormodel.cpp \
    colorpopupbutton.cpp \
    logger.cpp \
    loggingtarget.cpp \
    objectpool.cpp \
    settingsmanager.cpp

