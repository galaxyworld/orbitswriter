/*-------------------------------------------------
 *
 * OrbitsWriter - an Offline Blog Writer
 *
 * Copyright (C) 2012 devbean@galaxyworld.org
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 *-------------------------------------------------*/

#include <QDateTime>
#include <QList>
#include <QString>
#include <QReadWriteLock>
#include <QMutex>

#include "logger.h"
#include "global.h"

namespace Common
{
namespace Internal
{

static const char * LevelName[] = {
    "Trace",
    "Debug",
    "Info",
    "Warn",
    "Error",
    "Fatal"
};

static const char * loggingLevelName(LoggingLevel level)
{
    return LevelName[level];
}

static const QString DateTimeFormat("yyyy-MM-dd hh:mm:ss, zzz");

// ========== Common::Internal::LoggerPrivate ==========

//! The LoggerPrivate class is the private implementation of Logging.
class LoggerPrivate
{
public:
    LoggerPrivate() : loggingLevel(Common::Info) {}

    void log(LoggingLevel level, const QString & message)
    {
        if (loggingLevel <= level) {
            const QString msg(QString("[%1] [%2] - %3")
                              .arg(loggingLevelName(level),
                                   QDateTime::currentDateTime().toString(DateTimeFormat),
                                   message));
            lock.lockForWrite();
            foreach (Common::LoggingTarget * target, targets) {
                target->write(msg);
            }
        }
    }

    QReadWriteLock lock;
    LoggingLevel loggingLevel;
    QList<Common::LoggingTarget *> targets;

}; // end of class Common::Internal::LoggerPrivate

} // end of namespace Common::Internal

// ========== Common::Logger ==========

Logger * Logger::m_instance = 0;

Logger::Logger() :
    d(new Internal::LoggerPrivate)
{
}

Logger::~Logger()
{
    delete d;
}

Logger *Logger::instance()
{
    GET_INSTANCE(m_instance, Logger);
}

void Logger::setLogLevel(LoggingLevel level)
{
    d->loggingLevel = level;
}

LoggingLevel Logger::logLevel() const
{
    return d->loggingLevel;
}

void Logger::addLoggingTarget(LoggingTarget *target)
{
    d->targets.append(target);
}

void Logger::trace(const QString &message) const
{
    d->log(Trace, message);
}

void Logger::debug(const QString &message) const
{
    d->log(Debug, message);
}

void Logger::info(const QString &message) const
{
    d->log(Info, message);
}

void Logger::warn(const QString &message) const
{
    d->log(Warn, message);
}

void Logger::error(const QString &message) const
{
    d->log(Error, message);
}

void Logger::fatal(const QString &message) const
{
    d->log(Fatal, message);
}

} // end of namespace Common
