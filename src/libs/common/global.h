/*-------------------------------------------------
 *
 * OrbitsWriter - an Offline Blog Writer
 *
 * Copyright (C) 2012 devbean@galaxyworld.org
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 *-------------------------------------------------*/

#ifndef GLOBAL_H
#define GLOBAL_H

/*!
  This macto expands the singleton instance getter for
  \a class_name and the instance pointer name should be
  \a instance_name.

  You must include QMutex in order to use this macro
  meanwhile the class use it must have a default constructor.
 */
#define GET_INSTANCE(instance_name, class_name) \
    static QMutex mutex;                        \
    if (!instance_name) {                       \
        mutex.lock();                           \
        if (!instance_name) {                   \
            instance_name = new class_name;     \
        }                                       \
        mutex.unlock();                         \
    }                                           \
    return instance_name

#endif // GLOBAL_H
