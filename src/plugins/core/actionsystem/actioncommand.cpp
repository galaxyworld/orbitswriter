/*-------------------------------------------------
 *
 * OrbitsWriter - an Offline Blog Writer
 *
 * Copyright (C) 2012 devbean@galaxyworld.org
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 *-------------------------------------------------*/

#include <QAction>

#include "actioncommand.h"
#include "actioncommand_p.h"

using namespace Core;
using namespace Core::Internal;

// ========== Core::Internal::AbstractActionCommand ==========

AbstractActionCommand::AbstractActionCommand(UniqueIdentifier id) :
    m_id(id),
    m_isKeyInitialized(false),
    m_attributes(0)
{
}

UniqueIdentifier AbstractActionCommand::id() const
{
    return m_id;
}

void AbstractActionCommand::setDescription(const QString &text)
{
    m_defaultDesc = text;
}

QString AbstractActionCommand::description() const
{
    if (!m_defaultDesc.isEmpty()) {
        return m_defaultDesc;
    }
    if (action()) {
        QString text = action()->text();
        text.remove(QRegExp(QLatin1String("&(?!&)")));
        if (!text.isEmpty()) {
            return text;
        }
    }
    return id().toString();
}

QString AbstractActionCommand::stringWithAppendedShortcut(const QString &str) const
{
    return QString::fromLatin1("%1 <span style=\"color: gray; font-size: small\">%2</span>").
            arg(str, keySequence().toString(QKeySequence::NativeText));
}

void AbstractActionCommand::setDefaultKeySequence(const QKeySequence &key)
{
    if (!m_isKeyInitialized) {
        setKeySequence(key);
    }
    m_defaultKey = key;
}

QKeySequence AbstractActionCommand::defaultKeySequence() const
{
    return m_defaultKey;
}

void AbstractActionCommand::setKeySequence(const QKeySequence &key)
{
    Q_UNUSED(key)
    m_isKeyInitialized = true;
}

void AbstractActionCommand::setAttribute(CommandAttribute attr)
{
    m_attributes |= attr;
}

void AbstractActionCommand::removeAttribute(CommandAttribute attr)
{
    m_attributes &= ~attr;
}

bool AbstractActionCommand::hasAttribute(CommandAttribute attr) const
{
    return (m_attributes & attr);
}


// ========== Core::Internal::Action ==========

Action::Action(UniqueIdentifier id) :
    AbstractActionCommand(id),
    m_action(0),
    m_active(false)
{
}

void Action::setKeySequence(const QKeySequence &key)
{
    AbstractActionCommand::setKeySequence(key);
    m_action->setShortcut(key);
    emit keySequenceChanged();
}

QKeySequence Action::keySequence() const
{
    return m_action->shortcut();
}

void Action::setAction(QAction *action)
{
    m_action = action;
    if (m_action) {
        m_action->setParent(this);
        m_toolTip = m_action->toolTip();
        connect(m_action, SIGNAL(changed()), this, SLOT(updateActiveState()));
    }
}

QAction *Action::action() const
{
    return m_action;
}

bool Action::isActive() const
{
    return m_active;
}

void Action::updateActiveState()
{
    setActive(m_action->isEnabled() && m_action->isVisible() && !m_action->isSeparator());
}

void Action::updateToolTipWithKeySequence()
{
    if (m_action->shortcut().isEmpty()) {
        m_action->setToolTip(m_toolTip);
    } else {
        m_action->setToolTip(stringWithAppendedShortcut(m_toolTip));
    }
}

void Action::setActive(bool state)
{
    if (state != m_active) {
        m_active = state;
        emit activeStateChanged();
    }
}
