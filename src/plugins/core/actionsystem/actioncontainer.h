/*-------------------------------------------------
 *
 * OrbitsWriter - an Offline Blog Writer
 *
 * Copyright (C) 2012 devbean@galaxyworld.org
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 *-------------------------------------------------*/

#ifndef ACTIONCONTAINER_H
#define ACTIONCONTAINER_H

#include <QObject>

#include "../uniqueidentifier.h"

class QAction;
class QMenu;
class QMenuBar;
class QToolBar;

namespace Core
{

class ActionCommand;

/*!
  The ActionContainer class represents a menu, menu bar or tool bar.

  You should not create instances of this class directly, but use the
  ActionManager::createMenu() and ActionManager::createMenuBar() methods instead.
  Retrieves existing action containers for an ID with ActionManager::actionContainer().

  You could group menus and items together by defining groups, this groups are instances of
  <code>ActionGroup</code>. In order to define an action group,
  use ActionContainer::appendActionGroup() method then add menus/actions
  to these groups. If no custom groups are defined, an action container could add to three
  default groups Core::Constants::G_DEFAULT_ACTIONGROUP_1, Core::Constants::G_DEFAULT_ACTIONGROUP_2
  and Core::Constants::G_DEFAULT_ACTIONGROUP_3.

  Actions usually an instance of QAction. If a widget is needed, QWidgetAction
  could be used.
 */
class ActionContainer : public QObject
{
    Q_OBJECT
public:
    /*!
      Returns the id of this container.
     */
    virtual UniqueIdentifier id() const = 0;

    /*!
      Returns the QMenu instance that is represented by this action container,
      or 0 if this action container does not represent a menu.
     */
    virtual QMenu *menu() const = 0;

    /*!
      Returns the QMenuBar instance that is represented by this action container,
      or 0 if this action container does not represent a menu bar.
     */
    virtual QMenuBar *menuBar() const = 0;

    /*!
      Returns the QToolBar instance that is represented by this action container,
      or 0 if this action container does not represent a tool bar.
     */
    virtual QToolBar *toolBar() const = 0;

    /*!
      Adds an action group with the given identifier \a group to the action container.

      Using groups you can segment your action container into logical parts and add actions
      directly to these parts.

      \sa insertActionGroup()
      \sa addAction()
      \sa addMenu()
     */
    virtual void appendActionGroup(const UniqueIdentifier &group) = 0;

    /*!
      Inserts an action group with the given identifier \a group to the action container
      brfore the group \a before.

      \sa appendActionGroup()
     */
    virtual void insertActionGroup(const UniqueIdentifier &before, const UniqueIdentifier &group) = 0;

    /*!
      Returns an action representing the \a group.
     */
    virtual QAction * actionAt(const UniqueIdentifier &group) const = 0;

    /*!
      Adds the \a cmd to this action container.

      The action may be a menu item or a tool bar button, according to the container implemation.
      The action is added as the last item of the specified \a group.

      \sa appendActionGroup()
      \sa addMenu()
      \sa addWidget()
     */
    virtual void addAction(ActionCommand *cmd, const UniqueIdentifier &groupId = UniqueIdentifier()) = 0;

    /*!
      Adds the \a menu to this action container.

      The menu may be a submenu or a tool bar button with menu, according to the container implemation.
      The menu is added as the last item of the specified \a group.

      \sa appendActionGroup()
      \sa addAction()
      \sa addWidget()
     */
    virtual void addMenu(ActionContainer *menu, const UniqueIdentifier &group = UniqueIdentifier()) = 0;

    /*!
      Adds the \a menu to this action container.

      The menu may be a submenu or a tool bar button with menu, according to the container implemation.
      The menu is inserted before the position \a before of the specified \a group.

      \sa appendActionGroup()
      \sa addAction()
      \sa addWidget()
     */
    virtual void addMenu(ActionContainer *before, ActionContainer *menu,
                         const UniqueIdentifier &group = UniqueIdentifier()) = 0;

//    /*!
//      Adds \a widget to this action container.

//      The widget is added as the last item of the specified \a group.

//      \sa appendActionGroup()
//      \sa addAction()
//      \sa addMenu()
//     */
//    virtual void addWidget(QWidget * widget, const UniqueIdentifier &group = UniqueIdentifier()) = 0;

    /*!
      Adds a separator to the end of the given \a group to the action container.

      The created separator action is returned through \a outSeparator.

      Returns the created Command for the separator.
     */
    virtual ActionCommand * addSeparator(const UniqueIdentifier &group = UniqueIdentifier(),
                                         QAction **outSeparator = 0) = 0;
}; // end of class Core::ActionContainer

} // end of namespace Core

#endif // ACTIONCONTAINER_H
