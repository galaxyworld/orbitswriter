/*-------------------------------------------------
 *
 * OrbitsWriter - an Offline Blog Writer
 *
 * Copyright (C) 2012 devbean@galaxyworld.org
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 *-------------------------------------------------*/

#ifndef ACTIONMANAGER_H
#define ACTIONMANAGER_H

#include <QObject>

#include "actioncommand.h"
#include "../core_global.h"

namespace Core
{

class ActionContainer;
class MainWindow;
class UniqueIdentifier;

namespace Internal
{
class ActionManagerPrivate;
}

class CORE_EXPORT ActionManager : public QObject
{
    Q_OBJECT
public:
    static ActionManager * instance();

    /*!
      Creates a new menu bar if the menu bar with the given \a id does not exist,
      otherwise the existing one will return.

      Returns a new ActionContainer that you can use to get the QMenuBar instance
      or to add menus to the menu bar. The ActionManager owns the returned ActionContainer.
     */
    ActionContainer * createMenuBar(const UniqueIdentifier &id);

    /*!
      Creates a new menu if the menu with the given \a id does not exist,
      otherwise the existing one will return.

      Returns a new ActionContainer that you can use to get the QMenu instance
      or to add menu items to the menu. The ActionManager owns the returned ActionContainer.
      Add your menu to some other menu or a menu bar via the
      ActionManager::actionContainer() and ActionContainer::addMenu() methods.
    */
    ActionContainer * createMenu(const UniqueIdentifier &id);

    /*!
      Creates a new tool bar if the tool bar with the given \a id does not exist,
      otherwise the existing one will return.

      Returns a new ActionContainer that you can use to get the QToolBar instance
      or to add actions to the tool bar. The ActionManager owns the returned ActionContainer.
     */
    ActionContainer * createToolBar(const UniqueIdentifier &id);

    /*!
      Makes an \a action known to the system under the specified \a id.

      Returns an action command object that represents the action in the application and is
      owned by the ActionManager.

      This function will emit commandListChanged() and commandAdded(QString) signals.
     */
    ActionCommand * registerAction(QAction *action, const UniqueIdentifier &id);

    /*!
      Returns the ActionCommand object that is known to the system under the given \a id.

      \sa ActionManager::registerAction()
     */
    ActionCommand *actionCommand(const UniqueIdentifier &id) const;

    /*!
      Returns all action commands that have been registered.
     */
    QList<ActionCommand *> actionCommands() const;

    /*!
      Returns the ActionContainter object that is know to the system under the given \a id.

      \sa ActionManager::menu()
      \sa ActionManager::menuBar()
    */
    ActionContainer *actionContainer(const UniqueIdentifier &id) const;
    
signals:
    /*!
      Emits when command list changed, e.g. another action added into command list
      or remove a command from the list.
     */
    void commandListChanged();

    /*!
      Emits when a new command added with given \a id.
     */
    void commandAdded(const QString &id);

private:
    explicit ActionManager(QObject *parent = 0);
    ~ActionManager();
    Q_DISABLE_COPY(ActionManager)

    static ActionManager *m_instance;
    Internal::ActionManagerPrivate *d;
    friend class Internal::ActionManagerPrivate;
    friend class MainWindow;
}; // end of class Core::ActionManager

} // end of namespace Core

#endif // ACTIONMANAGER_H
