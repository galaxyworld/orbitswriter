/*-------------------------------------------------
 *
 * OrbitsWriter - an Offline Blog Writer
 *
 * Copyright (C) 2012 devbean@galaxyworld.org
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 *-------------------------------------------------*/

#ifndef ACTIONCONTAINER_P_H
#define ACTIONCONTAINER_P_H

#include <QList>

#include "actioncontainer.h"

namespace Core
{
namespace Internal
{

/*!
  Action group stands for a group that actions or action containers could
  add to.
 */
struct ActionGroup
{
    ActionGroup(const UniqueIdentifier &id) : id(id) {}
    UniqueIdentifier id;
    QList<QObject *> items; // ActionCommand * or ActionContainer *
}; // end of struct Core::Internal::ActionGroup


/*!
  The AbstractActionContainer class is the base for all action containers.

  This class supports management for action groups and other common functions.
  If special containers need be added, they should extends this base class.
 */
class AbstractActionContainer : public ActionContainer
{
    Q_OBJECT
public:
    AbstractActionContainer(UniqueIdentifier id);
    virtual ~AbstractActionContainer() {}

    virtual UniqueIdentifier id() const { return m_id; }

    virtual QMenu *menu() const { return 0; }
    virtual QMenuBar *menuBar() const { return 0; }
    virtual QToolBar *toolBar() const { return 0; }

    virtual QAction * actionAt(const UniqueIdentifier &group) const;
    virtual void appendActionGroup(const UniqueIdentifier &group);
    virtual void insertActionGroup(const UniqueIdentifier &before, const UniqueIdentifier &group);

    virtual void addAction(ActionCommand *cmd, const UniqueIdentifier &group = UniqueIdentifier());
    virtual void addMenu(ActionContainer *menu, const UniqueIdentifier &group = UniqueIdentifier());
    virtual void addMenu(ActionContainer *before, ActionContainer *menu, const UniqueIdentifier &group = UniqueIdentifier());
//    virtual void addWidget(QWidget * widget, const UniqueIdentifier &group = UniqueIdentifier());
    virtual ActionCommand * addSeparator(const UniqueIdentifier &group = UniqueIdentifier(), QAction **outSeparator = 0);

    virtual void insertAction(QAction *before, QAction *action) = 0;
    virtual void insertMenu(QAction *before, QMenu *menu) = 0;

    virtual void removeAction(QAction *action) = 0;
    virtual void removeMenu(QMenu *menu) = 0;

    virtual bool updateInternal() = 0;

protected:
    bool canAddAction(ActionCommand *cmd) const;
    virtual bool canBeAddedToMenu() const = 0;
    virtual bool canBeAddedToMenuBar() const = 0;
    QList<ActionGroup> m_groups;

private slots:
    void scheduleUpdate();
    void update();
    void itemDestroyed();

private:
    QList<ActionGroup>::const_iterator findActionGroup(const UniqueIdentifier &groupId) const;
    QAction * actionAt(QList<ActionGroup>::const_iterator group) const;

    UniqueIdentifier m_id;
    bool m_updateRequested;
}; // end of class Core::Internal::AbstractActionContainer


/*!
  The MenuBarActionContainer class is for menu bars.
 */
class MenuBarActionContainer : public AbstractActionContainer
{
    Q_OBJECT
public:
    explicit MenuBarActionContainer(UniqueIdentifier id);

    void setMenuBar(QMenuBar *menuBar);
    QMenuBar *menuBar() const;

    void insertAction(QAction *before, QAction *action);
    void insertMenu(QAction *before, QMenu *menu);

    void removeAction(QAction *action);
    void removeMenu(QMenu *menu);

protected:
    bool canBeAddedToMenu() const;
    bool canBeAddedToMenuBar() const;
    bool updateInternal();

private:
    QMenuBar *m_menuBar;
}; // end of class Core::Internal::MenuBarActionContainer


/*!
  The MenuActionContainer class is for menus.
 */
class MenuActionContainer : public AbstractActionContainer
{
    Q_OBJECT
public:
    explicit MenuActionContainer(UniqueIdentifier id);

    void setMenu(QMenu *menu);
    QMenu *menu() const;

    void insertAction(QAction *before, QAction *action);
    void insertMenu(QAction *before, QMenu *menu);

    void removeAction(QAction *action);
    void removeMenu(QMenu *menu);

protected:
    bool canBeAddedToMenu() const;
    bool canBeAddedToMenuBar() const;
    bool updateInternal();

private:
    QMenu *m_menu;
}; // end of class Core::Internal::MenuActionContainer


/*!
  The ToolBarActionContainer class is for menus.
 */
class ToolBarActionContainer : public AbstractActionContainer
{
    Q_OBJECT
public:
    explicit ToolBarActionContainer(UniqueIdentifier id);

    void setToolBar(QToolBar *toolBar);
    QToolBar *toolBar() const;

    void insertAction(QAction *before, QAction *action);
    void insertMenu(QAction *before, QMenu *menu);

    void removeAction(QAction *action);
    void removeMenu(QMenu *menu);

protected:
    bool canBeAddedToMenu() const;
    bool canBeAddedToMenuBar() const;
    bool updateInternal();

private:
    QToolBar *m_toolBar;
}; // end of class Core::Internal::ToolBarActionContainer

} // end of namespace Core::Internal
} // end of namespace Core

#endif // ACTIONCONTAINER_P_H
