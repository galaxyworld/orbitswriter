/*-------------------------------------------------
 *
 * OrbitsWriter - an Offline Blog Writer
 *
 * Copyright (C) 2012 devbean@galaxyworld.org
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 *-------------------------------------------------*/

#ifndef ACTIONCOMMAND_P_H
#define ACTIONCOMMAND_P_H

#include <QKeySequence>

#include "actioncommand.h"

namespace Core
{
namespace Internal
{

class AbstractActionCommand : public ActionCommand
{
    Q_OBJECT
public:
    AbstractActionCommand(UniqueIdentifier id);
    virtual ~AbstractActionCommand() {}

    UniqueIdentifier id() const;

    void setDescription(const QString &text);
    QString description() const;

    QString stringWithAppendedShortcut(const QString &str) const;

    QAction *action() const { return 0; }

    void setDefaultKeySequence(const QKeySequence &key);
    QKeySequence defaultKeySequence() const;
    void setKeySequence(const QKeySequence &key);

    void setAttribute(CommandAttribute attr);
    void removeAttribute(CommandAttribute attr);
    bool hasAttribute(CommandAttribute attr) const;

protected:
    UniqueIdentifier m_id;
    bool m_isKeyInitialized;
    CommandAttributes m_attributes;
    QString m_defaultDesc;
    QKeySequence m_defaultKey;
}; // end of class Core::Internal::AbstractActionCommand


class Action : public AbstractActionCommand
{
    Q_OBJECT
public:
    Action(UniqueIdentifier id);

    void setKeySequence(const QKeySequence &key);
    QKeySequence keySequence() const;

    void setAction(QAction *action);
    QAction *action() const;

    bool isActive() const;

private slots:
    void updateActiveState();

private:
    void updateToolTipWithKeySequence();
    void setActive(bool state);

    QAction *m_action;
    bool m_active;
    QString m_toolTip;
}; // end of class Core::Internal::Action

} // end of namespace Core::Internal
} // end of namespace Core

#endif // ACTIONCOMMAND_P_H
