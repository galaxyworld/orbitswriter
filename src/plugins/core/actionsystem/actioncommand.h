/*-------------------------------------------------
 *
 * OrbitsWriter - an Offline Blog Writer
 *
 * Copyright (C) 2012 devbean@galaxyworld.org
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 *-------------------------------------------------*/

#ifndef ACTIONCOMMAND_H
#define ACTIONCOMMAND_H

#include <QObject>

#include "../uniqueidentifier.h"

class QAction;

namespace Core
{

class ActionCommand : public QObject
{
    Q_OBJECT
public:
    /*!
      Defines how the user visible action is updated when the active action changes.

      The default is to update the enabled and visible state, and to disable the
      user visible action when there is no active action.
     */
    enum CommandAttribute {
        CA_Hide = 1,           //!< When there is no active action, hide the user "visible" action, instead of just
                               //!< disabling it.
        CA_UpdateText = 2,     //!< Also update the actions text.
        CA_UpdateIcon = 4,     //!< Also update the actions icon.
        CA_NonConfigurable = 8 //!< Flag to indicate that the keyboard shortcut of this Command should not be
                               //!< configurable by the user.
    };
    Q_DECLARE_FLAGS(CommandAttributes, CommandAttribute)

    /*!
      Returns the id of this ActionCommand.
     */
    virtual UniqueIdentifier id() const = 0;

    /*!
      Sets the \a text to represent the ActionCommand.

      If you don't set this, the current text from the user visible action is taken
      (which is OK in many cases).
     */
    virtual void setDescription(const QString &text) = 0;

    /*!
      Returns the text that is used to present this ActionCommand to the user.

      \sa setDescription()
     */
    virtual QString description() const = 0;

    /*!
      Returns the \a string with an appended representation of the keyboard shortcut
      that is currently assigned to this ActionCommand.
     */
    virtual QString stringWithAppendedShortcut(const QString &str) const = 0;

    /*!
      Returns the user visible action for this ActionCommand.
     */
    virtual QAction *action() const = 0;

    /*!
      Sets the default keyboard shortcut that should be used to activate thie ActionCommand.

      \sa defaultKeySequence()
     */
    virtual void setDefaultKeySequence(const QKeySequence &key) = 0;

    /*!
      Returns the default keyboard shortcut that can be used to activate this ActionCommand.

      \sa setDefaultKeySequence()
     */
    virtual QKeySequence defaultKeySequence() const = 0;

    /*!
      Returns the current keyboard shortcut assigned to this ActionCommand.

      \sa defaultKeySequence()
     */
    virtual QKeySequence keySequence() const = 0;

    /*!
      Sets shortcut for this ActionCommand.
     */
    virtual void setKeySequence(const QKeySequence &key) = 0;

    /*!
      Returns if the ActionCommand has an active action.
     */
    virtual bool isActive() const = 0;

    /*!
      Add the \a attribute to the attributes of this Command.

      \sa CommandAttribute
      \sa removeAttribute()
      \sa hasAttribute()
    */
    virtual void setAttribute(CommandAttribute attr) = 0;

    /*!
      Remove the \a attribute from the attributes of this Command.

      \sa CommandAttribute
      \sa setAttribute()
    */
    virtual void removeAttribute(CommandAttribute attr) = 0;

    /*!
      Returns if the Command has the \a attribute set.

      \sa CommandAttribute
      \sa removeAttribute()
      \sa setAttribute()
    */
    virtual bool hasAttribute(CommandAttribute attr) const = 0;

signals:
    /*!
      Emits when the keyboard shortcut assigned to this ActionCommand changes,
      e.g. when the user sets it in the keyboard shortcut settings dialog.
     */
    void keySequenceChanged();

    /*!
      Emits when the active state of this ActionCommand changes.
     */
    void activeStateChanged();
}; // end of class Core::ActionCommand

} // end of namespace Core

#endif // ACTIONCOMMAND_H
