/*-------------------------------------------------
 *
 * OrbitsWriter - an Offline Blog Writer
 *
 * Copyright (C) 2012 devbean@galaxyworld.org
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 *-------------------------------------------------*/

#include <QAction>
#include <QDebug>
#include <QMenuBar>
#include <QToolBar>
#include <QMutex>
#include <QSettings>

#include "appcore.h"
#include "actionmanager.h"
#include "actionmanager_p.h"
#include "common/global.h"

using namespace Core;
using namespace Core::Internal;


// ========== Core::ActionManager ==========

ActionManager * ActionManager::m_instance = 0;

ActionManager *ActionManager::instance()
{
    GET_INSTANCE(m_instance, ActionManager);
}

ActionManager::ActionManager(QObject *parent) :
    QObject(parent),
    d(new ActionManagerPrivate(this))
{
}

ActionManager::~ActionManager()
{
    delete d;
}

ActionContainer *ActionManager::createMenuBar(const UniqueIdentifier &id)
{
    const ActionManagerPrivate::IdContainerMap::const_iterator it = d->m_idContainerMap.constFind(id);
    if (it !=  d->m_idContainerMap.constEnd()) {
        return it.value();
    }

    QMenuBar *mb = new QMenuBar; // No parent (System menu bar on Mac OS X)
    mb->setObjectName(id.toString());

    MenuBarActionContainer *mbc = new MenuBarActionContainer(id);
    mbc->setMenuBar(mb);

    d->m_idContainerMap.insert(id, mbc);
    connect(mbc, SIGNAL(destroyed()), d, SLOT(containerDestroyed()));

    return mbc;
}

ActionContainer *ActionManager::createMenu(const UniqueIdentifier &id)
{
    const ActionManagerPrivate::IdContainerMap::const_iterator it = d->m_idContainerMap.constFind(id);
    if (it != d->m_idContainerMap.constEnd()) {
        return it.value();
    }

    QMenu *m = new QMenu(appCore->mainWindow());
    m->setObjectName(QLatin1String(id.name()));

    MenuActionContainer *mc = new MenuActionContainer(id);
    mc->setMenu(m);

    d->m_idContainerMap.insert(id, mc);
    connect(mc, SIGNAL(destroyed()), d, SLOT(containerDestroyed()));

    return mc;
}

ActionContainer *ActionManager::createToolBar(const UniqueIdentifier &id)
{
    const ActionManagerPrivate::IdContainerMap::const_iterator it = d->m_idContainerMap.constFind(id);
    if (it !=  d->m_idContainerMap.constEnd()) {
        return it.value();
    }

    QToolBar *tb = new QToolBar(appCore->mainWindow());
    tb->setObjectName(id.toString());

    ToolBarActionContainer *tbc = new ToolBarActionContainer(id);
    tbc->setToolBar(tb);

    d->m_idContainerMap.insert(id, tbc);
    connect(tbc, SIGNAL(destroyed()), d, SLOT(containerDestroyed()));

    return tbc;
}

ActionCommand *ActionManager::registerAction(QAction *action, const UniqueIdentifier &id)
{
    Action *act = d->createAction(id);
    if (act) {
        act->setAction(action);
        appCore->mainWindow()->addAction(action);
        action->setObjectName(id.toString());
        action->setShortcutContext(Qt::ApplicationShortcut);

        emit commandListChanged();
        emit commandAdded(id.toString());
    }
    return act;
}

ActionCommand *ActionManager::actionCommand(const UniqueIdentifier &id) const
{
    const ActionManagerPrivate::IdCommandMap::const_iterator it = d->m_idCommandMap.constFind(id);
    if (it == d->m_idCommandMap.constEnd()) {
        return 0;
    }
    return it.value();
}

QList<ActionCommand *> ActionManager::actionCommands() const
{
    QList<ActionCommand *> result;
    foreach (ActionCommand *cmd, d->m_idCommandMap.values()) {
        result << cmd;
    }
    return result;
}

ActionContainer *ActionManager::actionContainer(const UniqueIdentifier &id) const
{
    const ActionManagerPrivate::IdContainerMap::const_iterator it = d->m_idContainerMap.constFind(id);
    if (it == d->m_idContainerMap.constEnd()) {
        return 0;
    }
    return it.value();
}


// ========== Core::Internal::ActionManagerPrivate ==========

ActionManagerPrivate::ActionManagerPrivate(ActionManager *actionManager) :
    q(actionManager)
{
}

ActionManagerPrivate::~ActionManagerPrivate()
{
    // first delete containers to avoid them reacting to action deletion
    foreach (AbstractActionContainer *container, m_idContainerMap) {
        disconnect(container, SIGNAL(destroyed()), this, SLOT(containerDestroyed()));
    }
    qDeleteAll(m_idContainerMap.values());
    qDeleteAll(m_idCommandMap.values());
}

static const char settingsGroup[] = "KeyBindings";
static const char idKey[]         = "ID";
static const char sequenceKey[]   = "KeySequence";

void ActionManagerPrivate::initialize()
{
    QSettings *settings = appCore->settingsManager()->settings();
    const int shortcuts = settings->beginReadArray(QLatin1String(settingsGroup));
    for (int i = 0; i < shortcuts; ++i) {
        settings->setArrayIndex(i);
        const QKeySequence key(settings->value(QLatin1String(sequenceKey)).toString());
        const UniqueIdentifier id = UniqueIdentifier(settings->value(QLatin1String(idKey)).toString());
        ActionCommand *cmd = q->actionCommand(id);
        if (cmd) {
            cmd->setKeySequence(key);
        }
    }
    settings->endArray();
}

void ActionManagerPrivate::saveSettings(QSettings *settings)
{
    settings->beginWriteArray(QLatin1String(settingsGroup));
    int count = 0;

    const IdCommandMap::const_iterator cmdMapEnd = m_idCommandMap.constEnd();
    IdCommandMap::const_iterator it = m_idCommandMap.constBegin();
    while (it != cmdMapEnd) {
        const UniqueIdentifier id = it.key();
        AbstractActionCommand *cmd = it.value();
        QKeySequence key = cmd->keySequence();
        if (key != cmd->defaultKeySequence()) {
            settings->setArrayIndex(count);
            settings->setValue(QLatin1String(idKey), id.toString());
            settings->setValue(QLatin1String(sequenceKey), key.toString());
            count++;
        }
        ++it;
    }
    settings->endArray();
}

Action *ActionManagerPrivate::createAction(const UniqueIdentifier &id)
{
    Action *act = 0;
    if (AbstractActionCommand *cmd = m_idCommandMap.value(id, 0)) {
        act = qobject_cast<Action *>(cmd);
        if (!act) {
            qWarning() << "registerAction: id" << id.name()
                       << "is registered with a different command type.";
            return 0;
        }
    } else {
        act = new Action(id);
        m_idCommandMap.insert(id, act);
    }
    return act;
}

void ActionManagerPrivate::containerDestroyed()
{
    AbstractActionContainer *container = static_cast<AbstractActionContainer *>(sender());
    m_idContainerMap.remove(m_idContainerMap.key(container));
}
