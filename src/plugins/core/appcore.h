/*-------------------------------------------------
 *
 * OrbitsWriter - an Offline Blog Writer
 *
 * Copyright (C) 2012 devbean@galaxyworld.org
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 *-------------------------------------------------*/

#ifndef APPCORE_H
#define APPCORE_H

#include <QObject>

#include "core_global.h"
#include "documentmanager.h"
#include "actionsystem/actionmanager.h"
#include <common/logger.h>
#include <common/objectpool.h>
#include <common/settingsmanager.h>

/*!
  This macro expands to a global pointer referring to the unique core object.
  It is equivalent to the pointer returned by the AppCore::instance().
 */
#define appCore (Core::AppCore::instance())

/*!
  The Core namespace contains platform services, such as application
  data manager, action manager and logger.
 */
namespace Core
{

class MainWindow;

namespace Internal
{
class AppCorePrivate;
}

/*!
  The AppCore class allows access to the different part that make up
  the basic functionality of the whole application.

  You should never create a subclass of this class. The one and only
  instance is created by the application. You can access this instance
  from your plugin through AppCore::instance() or simply use appCore macro
  for short.
 */
class CORE_EXPORT AppCore : public QObject
{
    Q_OBJECT
public:
    /*!
      Gets the singleton instance of AppCore.
     */
    static AppCore * instance();

    // ---------- global singletons ---------- //
    /*!
      Returns the application's action manager.

      The action manager is responsible for registration of menus, menu items,
      tool bars and keyboard shortcuts.
     */
    ActionManager * actionManager() const
    {
        return ActionManager::instance();
    }

    /*!
      Returns the application's settings manager.
     */
    Common::SettingsManager * settingsManager() const
    {
        return Common::SettingsManager::instance();
    }

    /*!
      Returns the application's logger.
     */
    Common::Logger * logger() const
    {
        return Common::Logger::instance();
    }

    /*!
      Returns the application's object pool.
     */
    Common::ObjectPool * objectPool() const
    {
        return Common::ObjectPool::instance();
    }

    /*!
      Returns the application's documents manager.
     */
    DocumentManager * documentManager() const
    {
        return DocumentManager::instance();
    }

    /*!
      Returns the main window of this application.

      It is valid after core plugin initialized.
     */
    QWidget * mainWindow() const;

signals:
    /*!
      Emits when the core plugin is about to show.
     */
    void coreAboutToOpen();

    /*!
      Emits when the core plugin is about to close.
     */
    void coreAboutToClose();

    /*!
      Emits after all plugins have been loaded and the main window shown.
     */
    void coreOpened();

private:
    /*!
      Constructs an instance of AppCore.

      You should never call this constructor directly. Use AppCore::instance() instead.
     */
    explicit AppCore(QObject *parent = 0);
    /*!
      Destroys the instance of this class.
     */
    ~AppCore();
    Q_DISABLE_COPY(AppCore)

    static AppCore *m_instance;

    Internal::AppCorePrivate *d;
    friend class Internal::AppCorePrivate;
    friend class MainWindow;
}; // end of class Core::AppCore

} // end of namespace Core

#endif // APPCORE_H
