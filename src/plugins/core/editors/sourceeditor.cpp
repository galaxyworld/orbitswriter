/*-------------------------------------------------
 *
 * OrbitsWriter - an Offline Blog Writer
 *
 * Copyright (C) 2012 devbean@galaxyworld.org
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 *-------------------------------------------------*/

#include <QMenu>
#include <QPainter>
#include <QTextBlockUserData>

#include "appcore.h"
#include "coreconstants.h"
#include "sourceeditor.h"

namespace Core
{

namespace Internal
{

class LineData : public QTextBlockUserData
{
public:
    LineData() : QTextBlockUserData() {}
    ~LineData() {}

    bool isCurrentLine;
};

/*!
  \internal
  \brief Line number panel at the left side of source editor.
 */
class LineNumberPanel : public QWidget
{
public:
    LineNumberPanel(SourceEditor *parent) :
        QWidget(parent),
        m_sourceEditor(parent)
    {
    }

    QSize sizeHint() const
    {
        return QSize(m_sourceEditor->lineNumberPanelWidth(), 0);
    }

protected:
    void paintEvent(QPaintEvent *event)
    {
        m_sourceEditor->paintLineNumberPanel(event);
    }

private:
    SourceEditor *m_sourceEditor;
}; // end of class Widgets::LineNumberPanel

} // end of namespace Core::Internal

SourceEditor::SourceEditor(QWidget *parent) :
    QPlainTextEdit(parent),
    m_lineNumberPanel(new Internal::LineNumberPanel(this)),
    m_htmlHighlighter(new HtmlHighlighter(document()))
{
    setObjectName(QLatin1String(Constants::ON_SOURCEEDITOR));
    appCore->objectPool()->addObject(this);

    setContextMenuPolicy(Qt::CustomContextMenu);
    updateLineNumberPanelWidth(0);
    setLineNumbersEnable(true);

    // context menu actions
    QAction *seSeparatorAction = new QAction(this);
    seSeparatorAction->setSeparator(true);
    m_contextActions.append(seSeparatorAction);
    QAction *showLineNumberAction = new QAction(tr("Show Line Number"), this);
    showLineNumberAction->setCheckable(true);
    showLineNumberAction->setChecked(true);
    m_contextActions.append(showLineNumberAction);

    connect(this, SIGNAL(customContextMenuRequested(QPoint)),
            this, SLOT(showContextMenu(QPoint)));
    connect(this, SIGNAL(blockCountChanged(int)),
            this, SLOT(updateLineNumberPanelWidth(int)));
    connect(this, SIGNAL(updateRequest(QRect,int)),
            this, SLOT(updateLineNumberPanel(QRect,int)));
    connect(this, SIGNAL(cursorPositionChanged()),
            this, SLOT(highlightCurrentLine()));
    connect(showLineNumberAction, SIGNAL(toggled(bool)),
            this, SLOT(setLineNumbersEnable(bool)));
    connect(appCore->documentManager(), SIGNAL(documentContentChanged(QString)),
            this, SLOT(setPlainText(QString)));
}

SourceEditor::~SourceEditor()
{
}

void SourceEditor::setLineNumbersEnable(bool show)
{
    m_config.showLineNumbers = show;
    m_lineNumberPanel->setVisible(show);
    updateLineNumberPanelWidth(0);
}

void SourceEditor::resizeEvent(QResizeEvent *event)
{
    QPlainTextEdit::resizeEvent(event);

    QRect cr = contentsRect();
    m_lineNumberPanel->setGeometry(QRect(cr.left(), cr.top(),
                                         lineNumberPanelWidth(), cr.height()));
}

void SourceEditor::showContextMenu(const QPoint &pos)
{
    QPoint globalPos = viewport()->mapToGlobal(pos);
    QMenu *ctxMenu = createStandardContextMenu();
    ctxMenu->addActions(m_contextActions);
    ctxMenu->exec(globalPos);
}

void SourceEditor::updateLineNumberPanelWidth(int newBlockCount)
{
    Q_UNUSED(newBlockCount)
    setViewportMargins(lineNumberPanelWidth(), 0, 0, 0);
}

void SourceEditor::highlightCurrentLine()
{
    QList<QTextEdit::ExtraSelection> extraSelections;

    if (!isReadOnly()) {
        QTextEdit::ExtraSelection selection;
        QColor lineColor = QColor(Qt::yellow).lighter(160);

        selection.format.setBackground(lineColor);
        selection.format.setProperty(QTextFormat::FullWidthSelection, true);
        selection.cursor = textCursor();
        selection.cursor.clearSelection();
        extraSelections.append(selection);

        QTextBlock currentBlock = textCursor().block();
        Internal::LineData *currData = new Internal::LineData;
        currData->isCurrentLine = true;
        currentBlock.setUserData(currData);
        if (m_lastHighlightLine.isValid()) {
            m_lastHighlightLine.setUserData(0);
        }
        m_lastHighlightLine = currentBlock;
    }

    setExtraSelections(extraSelections);
}

void SourceEditor::updateLineNumberPanel(const QRect &rect, int dy)
{
    if (dy) {
        m_lineNumberPanel->scroll(0, dy);
    } else {
        m_lineNumberPanel->update(0, rect.y(), m_lineNumberPanel->width(), rect.height());
    }

    if (rect.contains(viewport()->rect())) {
        updateLineNumberPanelWidth(0);
    }
}

void SourceEditor::paintLineNumberPanel(QPaintEvent *event)
{
    if (!m_lineNumberPanel) {
        return;
    }

    QPainter painter(m_lineNumberPanel);
    painter.fillRect(event->rect(), Qt::lightGray);

    QTextBlock block = firstVisibleBlock();
    int blockNumber = block.blockNumber();
    int top = (int) blockBoundingGeometry(block).translated(contentOffset()).top();
    int bottom = top + (int) blockBoundingRect(block).height();

    while (block.isValid() && top <= event->rect().bottom()) {
        if (block.isVisible() && bottom >= event->rect().top()) {
            QString number = QString::number(blockNumber + 1);
            painter.setPen(Qt::black);
            Internal::LineData *data = (Internal::LineData *)block.userData();
            if (data && data->isCurrentLine) {
                painter.save();
                QFont font = painter.font();
                font.setBold(true);
                painter.setFont(font);
                painter.drawText(0, top, m_lineNumberPanel->width(), fontMetrics().height(),
                                 Qt::AlignRight, number);
                painter.restore();
            } else {
                painter.drawText(0, top, m_lineNumberPanel->width(), fontMetrics().height(),
                                 Qt::AlignRight, number);
            }
        }
        block = block.next();
        top = bottom;
        bottom = top + (int) blockBoundingRect(block).height();
        ++blockNumber;
    }
}

int SourceEditor::lineNumberPanelWidth() const
{
    int digits = 1;
    int max = qMax(1, blockCount());
    while (max >= 10) {
        max /= 10;
        ++digits;
    }
    return m_config.showLineNumbers
            ? 3 + fontMetrics().width(QLatin1Char('9')) * digits
            : 0;
}

} // end of namespace Core
