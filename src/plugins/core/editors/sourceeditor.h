/*-------------------------------------------------
 *
 * OrbitsWriter - an Offline Blog Writer
 *
 * Copyright (C) 2012 devbean@galaxyworld.org
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 *-------------------------------------------------*/

#ifndef SOURCEEDITOR_H
#define SOURCEEDITOR_H

#include <QPlainTextEdit>
#include <QTextBlock>

#include "htmlhighlighter.h"

namespace Core
{

namespace Internal
{
class LineNumberPanel;
} // end of namespace Core::Internal

/*!
  SourceEditor configuration.
 */
struct SourceEditorConfig
{
    bool showLineNumbers; //!< Shows line numbers or not.
};

/*!
  The HTML source editor used in OrbitsWriter.

  You could get its instance use
  \code
  appCore->objectPool()->getObject<Core::SourceEditor *>()
  \endcode
  .
 */
class SourceEditor : public QPlainTextEdit
{
    Q_OBJECT
public:
    explicit SourceEditor(QWidget *parent = 0);
    ~SourceEditor();
signals:
    
public slots:

    /*!
      Sets if the line number is enabled.
     */
    void setLineNumbersEnable(bool show);

protected:
    void resizeEvent(QResizeEvent *event);

private slots:
    void showContextMenu(const QPoint & pos);
    void updateLineNumberPanelWidth(int newBlockCount);
    void highlightCurrentLine();
    void updateLineNumberPanel(const QRect &rect, int dy);

private:
    void paintLineNumberPanel(QPaintEvent *event);
    int lineNumberPanelWidth() const;

    Internal::LineNumberPanel *m_lineNumberPanel;
//    HtmlSyntaxHighlighter *m_highlighter;
    QList<QAction *> m_contextActions;
    SourceEditorConfig m_config;
    QTextBlock m_lastHighlightLine;

    HtmlHighlighter *m_htmlHighlighter;

    friend class Internal::LineNumberPanel;
}; // end of class Core::SourceEditor

} // end of namespace Core

#endif // SOURCEEDITOR_H
