/*-------------------------------------------------
 *
 * OrbitsWriter - an Offline Blog Writer
 *
 * Copyright (C) 2012 devbean@galaxyworld.org
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 *-------------------------------------------------*/

#ifndef HTMLHIGHLIGHT_H
#define HTMLHIGHLIGHT_H

#include <QHash>
#include <QSyntaxHighlighter>

namespace Core
{

/*!
  \brief Html syntax highlighter.

  Based on <a href="http://doc.trolltech.com/qq/qq21-syntaxhighlighter.html">
  http://doc.trolltech.com/qq/qq21-syntaxhighlighter.html</a> and
  <a href="http://blog.qt.digia.com/2009/03/12/wysiwyg-html-editor/">
  http://blog.qt.digia.com/2009/03/12/wysiwyg-html-editor/</a>.
 */
class HtmlHighlighter : public QSyntaxHighlighter
{
    Q_OBJECT
public:
    /*!
      Constructs an instance of HtmlHighlighter with given
      \document as parent.
     */
    HtmlHighlighter(QTextDocument *document);

    /*!
      \brief HTML construct items.
     */
    enum Construct {
        DocType,
        Entity,
        Tag,
        Comment,
        AttributeName,
        AttributeValue
    };

protected:
    enum State {
        State_Text = -1,
        State_DocType,
        State_Comment,
        State_TagStart,
        State_TagName,
        State_InsideTag,
        State_AttributeName,
        State_SingleQuote,
        State_DoubleQuote,
        State_AttributeValue
    };

    void highlightBlock(const QString &text);

private:
    QHash<int, QColor> m_colors;
}; // end of class Core::HtmlHighlight

} // end of namespace Core

#endif // HTMLHIGHLIGHT_H
