/*-------------------------------------------------
 *
 * OrbitsWriter - an Offline Blog Writer
 *
 * Copyright (C) 2012 devbean@galaxyworld.org
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 *-------------------------------------------------*/

#include <QAction>
#include <QWebFrame>

#include "appcore.h"
#include "coreconstants.h"
#include "visualeditor.h"

using namespace Core;

VisualEditor::VisualEditor(QWidget *parent) :
    QWebView(parent)
{
    setObjectName(QLatin1String(Constants::ON_VISUALEDITOR));
    appCore->objectPool()->addObject(this);

    page()->setContentEditable(true);

    connect(page(), SIGNAL(contentsChanged()), SIGNAL(contentsChanged()));
    connect(appCore->documentManager(), SIGNAL(documentContentChanged(QString)),
            this, SLOT(setHtmlContent(QString)));
}

VisualEditor::~VisualEditor()
{
}

QString VisualEditor::toFormattedHtml() const
{
    return page()->mainFrame()->toHtml();
}

void VisualEditor::setHtmlContent(const QString &html)
{
    setHtml(html);
}

void VisualEditor::execCommand(const QString &cmd)
{
    QWebFrame *frame = page()->mainFrame();
    QString js = QString("document.execCommand(\"%1\", false, null)").arg(cmd);
    frame->evaluateJavaScript(js);
}

void VisualEditor::execCommand(const QString &cmd, const QString &arg)
{
    QWebFrame *frame = page()->mainFrame();
    QString js = QString("document.execCommand(\"%1\", false, \"%2\")").arg(cmd, arg);
    frame->evaluateJavaScript(js);
}

bool VisualEditor::queryCommandState(const QString &cmd)
{
    QWebFrame *frame = page()->mainFrame();
    QString js = QString("document.queryCommandState(\"%1\", false, null)").arg(cmd);
    QVariant result = frame->evaluateJavaScript(js);
    return result.toString().simplified().toLower() == "true";
}
