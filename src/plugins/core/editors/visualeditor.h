/*-------------------------------------------------
 *
 * OrbitsWriter - an Offline Blog Writer
 *
 * Copyright (C) 2012 devbean@galaxyworld.org
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 *-------------------------------------------------*/

#ifndef VISUALEDITOR_H
#define VISUALEDITOR_H

#include <QWebView>

#include "../core_global.h"

namespace Core
{

/*!
  The WYSIWYG HTML editor used in OrbitsWriter.

  You could get its instance use
  \code
  appCore->objectPool()->getObject<Core::VisualEditor *>()
  \endcode
  .
 */
class CORE_EXPORT VisualEditor : public QWebView
{
    Q_OBJECT
public:
    /*!
      Constructs an instance of VisualEditor.
     */
    explicit VisualEditor(QWidget *parent = 0);

    /*!
      Destructs the instance of VisualEditor.
     */
    ~VisualEditor();

    /*!
      Returns formatted HTML code of this page.
     */
    QString toFormattedHtml() const;

signals:
    /*!
      Emits when editor contents changed.
     */
    void contentsChanged();

public slots:

    /*!
      Sets HTML code to content.
     */
    void setHtmlContent(const QString &html);

    /*!
      Executes a JavaScript command \a cmd.

      This function will invoke JavaScript function
      \code
      document.execCommand(cmd, false, null)
      \endcode
      .
     */
    void execCommand(const QString & cmd);

    /*!
      Executes a JavaScript command \a cmd with argument \a arg.

      This function will invoke JavaScript function
      \code
      document.execCommand(cmd, false, arg)
      \endcode
      .
     */
    void execCommand(const QString & cmd, const QString & arg);

    /*!
      Querys state of a command \a cmd.

      This function will invoke JavaScript function
      \code
      document.queryCommandState(cmd, false, null)
      \endcode
      .
     */
    bool queryCommandState(const QString & cmd);
}; // end of class Core::VisualEditor

} // end of namespace Core

#endif // VISUALEDITOR_H
