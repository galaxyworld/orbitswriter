/*-------------------------------------------------
 *
 * OrbitsWriter - an Offline Blog Writer
 *
 * Copyright (C) 2012 devbean@galaxyworld.org
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 *-------------------------------------------------*/

#ifndef UNIQUEIDENTIFIER_H
#define UNIQUEIDENTIFIER_H

#include <QMetaType>
#include <QString>

#include "core_global.h"

namespace Core
{

/*!
  The class UniqueIdentifier encapsulates an identifier.

  It is used as a type-safe helper class instead of a \c QString or \c QByteArray.
  The internal representation of the id is assumed to be plain 7-bit-clean ASCII.
 */
class CORE_EXPORT UniqueIdentifier
{
public:
    /*!
      Constructs an instance of UniqueIdentifier from
      the unique identifier \a uid.
     */
    static UniqueIdentifier fromUniqueIdentifier(int uid)
    {
        return UniqueIdentifier(uid, uid);
    }

    /*!
      Constructs an invalid instance of UniqueIdentifier.
     */
    UniqueIdentifier() : m_uid(0) {}

    /*!
      Constants an instance of UniqueIdentifier with \a name.
     */
    UniqueIdentifier(const char *name);

    /*!
      Constants an instance of UniqueIdentifier with \a name.
     */
    explicit UniqueIdentifier(const QString &name);

    /*!
      Gets the name of this UniqueIdentifier.
     */
    QByteArray name() const;

    /*!
      Returns the string format of this UniqueIdentifier.
     */
    QString toString() const;

    /*!
      Returns true if this UniqueIdentifier is valid.
     */
    bool isValid() const
    {
        return m_uid;
    }

    /*!
      Returns the unique identifier of this Id.
     */
    int uniqueIdentifier() const
    {
        return m_uid;
    }

    /*!
      Returns true if this UniqueIdentifier is equal to the other one.
     */
    bool operator==(UniqueIdentifier id) const
    {
        return m_uid == id.m_uid;
    }

    /*!
      Returns true if this UniqueIdentifier is equal to the one with \a name.
     */
    bool operator==(const char *name) const;

    /*!
      Returns true if this UniqueIdentifier is not equal to the other one.
     */
    bool operator!=(UniqueIdentifier id) const
    {
        return m_uid != id.m_uid;
    }

    /*!
      Returns true if this Id is not equal to the one with \a name.
     */
    bool operator!=(const char *name) const
    {
        return !operator==(name);
    }

    /*!
      Returns true if this string is less than the other one.
     */
    bool operator<(UniqueIdentifier id) const
    {
        return m_uid < id.m_uid;
    }

    /*!
      Returns true if this string is greater than the other one.
     */
    bool operator>(UniqueIdentifier id) const
    {
        return m_uid > id.m_uid;
    }

private:
    int m_uid;

    UniqueIdentifier(int uid, int) : m_uid(uid) {}
    UniqueIdentifier(const QLatin1String &); // Intentionally unimplemented
}; // end of class Core::UniqueIdentifier

//! Hash function for Id.
inline uint qHash(const UniqueIdentifier &id) { return id.uniqueIdentifier(); }

} // end of namespace Core

Q_DECLARE_METATYPE(Core::UniqueIdentifier)
Q_DECLARE_METATYPE(QList<Core::UniqueIdentifier>)

#endif // UNIQUEIDENTIFIER_H
