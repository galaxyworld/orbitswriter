/*-------------------------------------------------
 *
 * OrbitsWriter - an Offline Blog Writer
 *
 * Copyright (C) 2012 devbean@galaxyworld.org
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 *-------------------------------------------------*/

#include <QtPlugin>
#include "coreplugin.h"
#include "mainwindow.h"

using namespace Core;

CorePlugin::CorePlugin() :
    m_mainWindow(new MainWindow)
{
}

CorePlugin::~CorePlugin()
{
    delete m_mainWindow;
}

bool CorePlugin::initialize(const QStringList &arguments, QString *errorString)
{
    Q_UNUSED(arguments)
    const bool success = m_mainWindow->init(errorString);
    return success;
}

void CorePlugin::dependenciesInitialized()
{
    m_mainWindow->dependenciesInitialized();
}

Q_EXPORT_PLUGIN2(CorePlugin, Core::CorePlugin)
