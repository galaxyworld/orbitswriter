/*-------------------------------------------------
 *
 * OrbitsWriter - an Offline Blog Writer
 *
 * Copyright (C) 2012 devbean@galaxyworld.org
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 *-------------------------------------------------*/

#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>

#include "core_global.h"

class QLineEdit;

namespace Core
{

class DocumentManager;
class SourceEditor;
class VisualEditor;

/*!
  The MainWindow class is the main window of OrbitsWriter.

  You could not create any instance of this class. If you want to get the only instance,
  use
  \code
  appCore->mainWindow()
  \endcode
  or
  \code
  qobject_cast<Core::MainWindow *>(appCore->mainWindow())
  \endcode
  .

  Plugins will be loaded after main window shown. So you could use any action groups
  or widgets in main window.
 */
class CORE_EXPORT MainWindow : public QMainWindow
{
    Q_OBJECT
public:
    /*!
      Constructs an instance of MainWindow.
     */
    explicit MainWindow(QWidget *parent = 0);

    /*!
      Destructs the instance of MainWindow.
     */
    ~MainWindow();
	
	bool init(QString *errorMessage);
    void dependenciesInitialized();
	
protected:
    void closeEvent(QCloseEvent *event);

private slots:
    void adjustActions();
    void editorChanged(int index);

private:
    void registerDefaultContainers();
    void registerDefaultActions();
    void addEditArea();

    void readSettings();
    void writeSettings();

    // Actions
    QAction *m_newDocAction;
    QAction *m_openDocAction;
    QAction *m_exitAction;
    QAction *m_undoAction;
    QAction *m_redoAction;
    QAction *m_cutAction;
    QAction *m_copyAction;
    QAction *m_pasteAction;

    // Widgets
    QLineEdit    *m_titleEdit;
    VisualEditor *m_visualEditor;
    SourceEditor *m_sourceEditor;
}; // end of class Core::MainWindow

} // end of namespace Core

#endif // MAINWINDOW_H
