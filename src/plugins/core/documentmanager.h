/*-------------------------------------------------
 *
 * OrbitsWriter - an Offline Blog Writer
 *
 * Copyright (C) 2012 devbean@galaxyworld.org
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 *-------------------------------------------------*/

#ifndef DOCUMENTMANAGER_H
#define DOCUMENTMANAGER_H

#include <QList>
#include <QObject>

namespace Core
{

class Document;

/*!
  The DocumentManager class is the manager for documents.

  \sa Document
 */
class DocumentManager : public QObject
{
    Q_OBJECT
public:
    /*!
      Gets the singleton instance of DocumentManager.
     */
    static DocumentManager * instance();

    /*!
      Writes \a content to current document's content.
     */
    void writeCurrentDocument(const QString &content);

    /*!
      Returns current document of this application.

      If there is no any processing document, 0 will be returned.
     */
    Document * currentDocument() const;

signals:
    void documentContentChanged(const QString &content);

public slots:
    /*!
      Creates a new document and append it to documents list.
     */
    Document *createDocument();
    
private:
    explicit DocumentManager();
    ~DocumentManager();

    static DocumentManager *m_instance;
    QList<Document *> m_docList;
}; // end of class Core::DocumentManager

} // end of namespace Core

#endif // DOCUMENTMANAGER_H
