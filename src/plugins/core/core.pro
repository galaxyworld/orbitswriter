#-------------------------------------------------
#
# OrbitsWriter - an Offline Blog Writer
#
# Copyright (C) 2012 devbean@galaxyworld.org
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
#-------------------------------------------------

TEMPLATE = lib
TARGET   = Core
DEFINES += CORE_LIBRARY

QT      *= core gui webkit
greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

include(../../plugin.pri)
include(core_dependencies.pri)

HEADERS += \
    actionsystem/actioncommand.h \
    actionsystem/actioncommand_p.h \
    actionsystem/actioncontainer.h \
    actionsystem/actioncontainer_p.h \
    actionsystem/actionmanager.h \
    actionsystem/actionmanager_p.h \
    editors/visualeditor.h \
    editors/sourceeditor.h \
    appcore.h \
    appcore_p.h \
    core_global.h \
    coreconstants.h \
    corelistener.h \
    coreplugin.h \
    mainwindow.h \
    uniqueidentifier.h \
    document.h \
    documentmanager.h \
    editors/htmlhighlighter.h

SOURCES += \
    actionsystem/actioncommand.cpp \
    actionsystem/actioncontainer.cpp \
    actionsystem/actionmanager.cpp \
    editors/visualeditor.cpp \
    editors/sourceeditor.cpp \
    appcore.cpp \
    coreplugin.cpp \
    mainwindow.cpp \
    uniqueidentifier.cpp \
    document.cpp \
    documentmanager.cpp \
    editors/htmlhighlighter.cpp

RESOURCES += \
    resources/resources.qrc
