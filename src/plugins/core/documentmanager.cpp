/*-------------------------------------------------
 *
 * OrbitsWriter - an Offline Blog Writer
 *
 * Copyright (C) 2012 devbean@galaxyworld.org
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 *-------------------------------------------------*/

#include <QMutex>

#include "document.h"
#include "documentmanager.h"
#include "common/global.h"

using namespace Core;

DocumentManager *DocumentManager::m_instance = 0;

DocumentManager *DocumentManager::instance()
{
    GET_INSTANCE(m_instance, DocumentManager);
}

DocumentManager::DocumentManager()
{
}

DocumentManager::~DocumentManager()
{
}

void DocumentManager::writeCurrentDocument(const QString &content)
{
    Document *currDoc = currentDocument();
    if (currDoc) {
        currDoc->setContent(content);
    }
}

Document *DocumentManager::currentDocument() const
{
    if (!m_docList.isEmpty()) {
        return m_docList.last();
    } else {
        return 0;
    }
}

Document *DocumentManager::createDocument()
{
    Document *doc = new Document(this);
    m_docList.append(doc);
    connect(doc, SIGNAL(contentChanged(QString)), SIGNAL(documentContentChanged(QString)));
    doc->init();
    return doc;
}
