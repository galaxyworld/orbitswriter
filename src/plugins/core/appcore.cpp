/*-------------------------------------------------
 *
 * OrbitsWriter - an Offline Blog Writer
 *
 * Copyright (C) 2012 devbean@galaxyworld.org
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 *-------------------------------------------------*/

#include <QMutex>

#include "appcore.h"
#include "appcore_p.h"
#include "common/global.h"

using namespace Core;
using namespace Core::Internal;

// ========== Core::AppCore ==========

AppCore * AppCore::m_instance = 0;

AppCore * AppCore::instance()
{
    GET_INSTANCE(m_instance, AppCore);
}

AppCore::AppCore(QObject *parent) :
    QObject(parent),
    d(new AppCorePrivate(this))
{
}

AppCore::~AppCore()
{
    delete d;
}

QWidget *AppCore::mainWindow() const
{
    return d->mainWindow;
}


// ========== Core::Internal::AppCorePrivate ==========

AppCorePrivate::AppCorePrivate(AppCore *ac) :
    q(ac)
{
}
