/*-------------------------------------------------
 *
 * OrbitsWriter - an Offline Blog Writer
 *
 * Copyright (C) 2012 devbean@galaxyworld.org
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 *-------------------------------------------------*/

#include <QAction>
#include <QCloseEvent>
#include <QDebug>
#include <QLineEdit>
#include <QMenu>
#include <QTabWidget>
#include <QToolBar>
#include <QVBoxLayout>

#include "coreconstants.h"
#include "corelistener.h"
#include "appcore.h"
#include "appcore_p.h"
#include "documentmanager.h"
#include "mainwindow.h"
#include "actionsystem/actioncontainer.h"
#include "actionsystem/actionmanager_p.h"
#include "editors/sourceeditor.h"
#include "editors/visualeditor.h"

using namespace Core;

#define FORWARD_ACTION(a1, a2) \
    connect(a1, SIGNAL(triggered()), \
            m_visualEditor->pageAction(a2), SLOT(trigger())); \
    connect(m_visualEditor->pageAction(a2), \
            SIGNAL(changed()), SLOT(adjustActions()));

#define FOLLOW_ENABLE(a1, a2) a1->setEnabled(m_visualEditor->pageAction(a2)->isEnabled())
#define FOLLOW_CHECK(a1, a2) a1->setChecked(m_visualEditor->pageAction(a2)->isChecked())

static const int IDX_VISUALEDITOR = 0;
static const int IDX_SOURCEEDITOR = 1;

// Icons
static const char ICON_NEWDOC[]       = ":/images/new_doc";      //! Path for new document icon.
static const char ICON_OPENDOC[]      = ":/images/open";         //! Path for open document icon.
static const char ICON_SAVEDOC[]      = ":/images/save";         //! Path for save document icon.
static const char ICON_SAVEASDOC[]    = ":/images/save_as";      //! Path for save as document icon.
static const char ICON_UNDO[]         = ":/images/undo";         //! Path for undo icon.
static const char ICON_REDO[]         = ":/images/redo";         //! Path for redo icon.
static const char ICON_CUT[]          = ":/images/cut";          //! Path for cut icon.
static const char ICON_COPY[]         = ":/images/copy";         //! Path for copy icon.
static const char ICON_PASTE[]        = ":/images/paste";        //! Path for paste icon.
static const char ICON_OPTION[]       = ":/images/option";       //! Path for option icon.
static const char ICON_HELPCONTENTS[] = ":/images/helpcontents"; //! Path for help contents icon.
static const char ICON_ABOUT[]        = ":/images/about";        //! Path for about icon.

MainWindow::MainWindow(QWidget *parent) :
    QMainWindow(parent),
    m_newDocAction(0),
    m_openDocAction(0),
    m_exitAction(0),
    m_undoAction(0),
    m_redoAction(0),
    m_cutAction(0),
    m_copyAction(0),
    m_pasteAction(0),
    m_titleEdit(0),
    m_visualEditor(0),
    m_sourceEditor(0)

{
    appCore->d->mainWindow = this;

    setWindowTitle(tr("Orbits Writer [*]"));
    setUnifiedTitleAndToolBarOnMac(true);
}

MainWindow::~MainWindow()
{
}

void MainWindow::closeEvent(QCloseEvent *event)
{
    const QList<CoreListener *> listeners = appCore->objectPool()->getObjects<CoreListener>();
    foreach (CoreListener *listener, listeners) {
        if (!listener->mainWindowAboutToClose()) {
            event->ignore();
            return;
        }
    }
    emit appCore->coreAboutToClose();
    writeSettings();
    event->accept();
}

bool MainWindow::init(QString *errorMessage)
{
    Q_UNUSED(errorMessage)
    registerDefaultContainers();
    registerDefaultActions();
    addEditArea();
    return true;
}

void MainWindow::dependenciesInitialized()
{
    ActionManager *actionManager = appCore->actionManager();
    actionManager->d->initialize();

    readSettings();

    DocumentManager *documentManager = appCore->documentManager();
    documentManager->createDocument();

    emit appCore->coreAboutToOpen();
    show();
    emit appCore->coreOpened();
}

void MainWindow::registerDefaultContainers()
{
    ActionManager *actionManager = appCore->actionManager();
    // tool bar
    ActionContainer *toolBar = actionManager->createToolBar(Constants::DEFAULT_TOOL_BAR);
    addToolBar(toolBar->toolBar());
    toolBar->toolBar()->setFloatable(false);
    toolBar->toolBar()->setMovable(false);
    toolBar->appendActionGroup(Constants::G_FILE);
    toolBar->appendActionGroup(Constants::G_EDIT_UNDOREDO);
    toolBar->appendActionGroup(Constants::G_EDIT_COPYPASTE);
    toolBar->appendActionGroup(Constants::G_FORMAT_TEXTSTYLE);
    toolBar->appendActionGroup(Constants::G_FORMAT_FONTCOLOR);
    toolBar->appendActionGroup(Constants::G_FORMAT_ALIGNMENT);

    // menu bar
    ActionContainer *menuBar = actionManager->createMenuBar(Constants::MENU_BAR);
#ifndef Q_OS_MAC // System menu bar on Mac
    setMenuBar(menuBar->menuBar());
#endif
    menuBar->appendActionGroup(Constants::G_FILE);
    menuBar->appendActionGroup(Constants::G_EDIT);
    menuBar->appendActionGroup(Constants::G_FORMAT);
    menuBar->appendActionGroup(Constants::G_INSERT);
    menuBar->appendActionGroup(Constants::G_TOOLS);
    menuBar->appendActionGroup(Constants::G_HELP);

    // File Menu
    ActionContainer *fileMenu = actionManager->createMenu(Constants::M_FILE);
    menuBar->addMenu(fileMenu, Constants::G_FILE);
    fileMenu->menu()->setTitle(tr("&File"));
    fileMenu->appendActionGroup(Constants::G_FILE_NEW);
    fileMenu->appendActionGroup(Constants::G_FILE_OPEN);
    fileMenu->appendActionGroup(Constants::G_FILE_SAVE);
    fileMenu->appendActionGroup(Constants::G_FILE_CLOSE);
    fileMenu->appendActionGroup(Constants::G_FILE_OTHER);

    // Edit Menu
    ActionContainer *editMenu = actionManager->createMenu(Constants::M_EDIT);
    menuBar->addMenu(editMenu, Constants::G_EDIT);
    editMenu->menu()->setTitle(tr("&Edit"));
    editMenu->appendActionGroup(Constants::G_EDIT_UNDOREDO);
    editMenu->appendActionGroup(Constants::G_EDIT_COPYPASTE);
////    editMenu->appendActionGroup(Constants::P_EDIT_ADVANCED);
////    editMenu->appendActionGroup(Constants::P_EDIT_FIND);
    editMenu->appendActionGroup(Constants::G_EDIT_OTHER);

	// Format Menu
    ActionContainer *formatMenu = actionManager->createMenu(Constants::M_FORMAT);
    menuBar->addMenu(formatMenu, Constants::G_FORMAT);
    formatMenu->menu()->setTitle(tr("F&ormat"));
    formatMenu->appendActionGroup(Constants::G_FORMAT_TEXTSTYLE);
    formatMenu->appendActionGroup(Constants::G_FORMAT_FONTCOLOR);
	formatMenu->appendActionGroup(Constants::G_FORMAT_ALIGNMENT);
	formatMenu->appendActionGroup(Constants::G_FORMAT_OTHER);

    // Insert Menu
    ActionContainer *insertMenu = actionManager->createMenu(Constants::M_INSERT);
    menuBar->addMenu(insertMenu, Constants::G_INSERT);
    insertMenu->menu()->setTitle(tr("&Insert"));
    insertMenu->appendActionGroup(Constants::G_INSERT_OTHER);

    // Tools Menu
    ActionContainer *toolsMenu = actionManager->createMenu(Constants::M_TOOLS);
    menuBar->addMenu(toolsMenu, Constants::G_TOOLS);
    toolsMenu->menu()->setTitle(tr("&Tools"));
    toolsMenu->appendActionGroup(Constants::G_TOOLS_OPTION);

    // Help Menu
    ActionContainer *helpMenu = actionManager->createMenu(Constants::M_HELP);
    menuBar->addMenu(helpMenu, Constants::G_HELP);
    helpMenu->menu()->setTitle(tr("&Help"));
    helpMenu->appendActionGroup(Constants::G_HELP_HELPCONTENTS);
    helpMenu->appendActionGroup(Constants::G_HELP_ABOUT);
}

void MainWindow::registerDefaultActions()
{
    ActionManager *actionManager = appCore->actionManager();
    ActionContainer *fileMenu = actionManager->actionContainer(Constants::M_FILE);
    ActionContainer *editMenu = actionManager->actionContainer(Constants::M_EDIT);
	ActionContainer *formatMenu = actionManager->actionContainer(Constants::M_FORMAT);
	ActionContainer *insertMenu = actionManager->actionContainer(Constants::M_INSERT);
    ActionContainer *toolsMenu = actionManager->actionContainer(Constants::M_TOOLS);
    ActionContainer *helpMenu = actionManager->actionContainer(Constants::M_HELP);
    ActionContainer *toolBar = actionManager->actionContainer(Constants::DEFAULT_TOOL_BAR);

    // Tool Bar Separators
    toolBar->addSeparator(Constants::G_EDIT_UNDOREDO);
    toolBar->addSeparator(Constants::G_EDIT_COPYPASTE);
    toolBar->addSeparator(Constants::G_FORMAT_TEXTSTYLE);
    toolBar->addSeparator(Constants::G_FORMAT_FONTCOLOR);
    toolBar->addSeparator(Constants::G_FORMAT_ALIGNMENT);
    // File Menu Separators
    fileMenu->addSeparator(Constants::G_FILE_SAVE);
    fileMenu->addSeparator(Constants::G_FILE_OTHER);
    // Edit Menu Separators
    editMenu->addSeparator(Constants::G_EDIT_COPYPASTE);
	// Format Menu Separators
    formatMenu->addSeparator(Constants::G_FORMAT_FONTCOLOR);
    formatMenu->addSeparator(Constants::G_FORMAT_ALIGNMENT);
	formatMenu->addSeparator(Constants::G_FORMAT_OTHER);
	// Insert Menu Separators
    insertMenu->addSeparator(Constants::G_INSERT_OTHER);
    // Help Menu Separators
    helpMenu->addSeparator(Constants::G_HELP_ABOUT);

    // New File Action
    QIcon icon = QIcon::fromTheme(QLatin1String("document-new"), QIcon(QLatin1String(ICON_NEWDOC)));
    m_newDocAction = new QAction(icon, tr("&New"), this);
    ActionCommand *cmd = actionManager->registerAction(m_newDocAction, Constants::ID_NEWDOC);
    cmd->setDefaultKeySequence(QKeySequence::New);
    fileMenu->addAction(cmd, Constants::G_FILE_NEW);
    toolBar->addAction(cmd, Constants::G_FILE);
    // connect(m_newAction, SIGNAL(triggered()), this, SLOT(newFile()));

    // Open Action
    icon = QIcon::fromTheme(QLatin1String("document-open"), QIcon(QLatin1String(ICON_OPENDOC)));
    m_openDocAction = new QAction(icon, tr("&Open..."), this);
    cmd = actionManager->registerAction(m_openDocAction, Constants::ID_OPENDOC);
    cmd->setDefaultKeySequence(QKeySequence::Open);
    fileMenu->addAction(cmd, Constants::G_FILE_OPEN);
    toolBar->addAction(cmd, Constants::G_FILE);
    // connect(m_openAction, SIGNAL(triggered()), this, SLOT(openFile()));

    // Save Action
    icon = QIcon::fromTheme(QLatin1String("document-save"), QIcon(QLatin1String(ICON_SAVEDOC)));
    QAction *tmpAction = new QAction(icon, tr("&Save"), this);
    tmpAction->setEnabled(false);
    cmd = actionManager->registerAction(tmpAction, Constants::ID_SAVEDOC);
    cmd->setDefaultKeySequence(QKeySequence::Save);
    cmd->setAttribute(ActionCommand::CA_UpdateText);
    cmd->setDescription(tr("Save"));
    fileMenu->addAction(cmd, Constants::G_FILE_SAVE);
    toolBar->addAction(cmd, Constants::G_FILE);

    // Save As Action
    icon = QIcon::fromTheme(QLatin1String("document-save-as"), QIcon(QLatin1String(ICON_SAVEASDOC)));
    tmpAction = new QAction(icon, tr("Save &As..."), this);
    tmpAction->setEnabled(false);
    cmd = actionManager->registerAction(tmpAction, Constants::ID_SAVEASDOC);
    cmd->setDefaultKeySequence(QKeySequence::SaveAs);
    cmd->setAttribute(ActionCommand::CA_UpdateText);
    cmd->setDescription(tr("Save As..."));
    fileMenu->addAction(cmd, Constants::G_FILE_SAVE);

    // Exit Action
    icon = QIcon::fromTheme(QLatin1String("application-exit"));
    m_exitAction = new QAction(icon, tr("E&xit"), this);
    cmd = actionManager->registerAction(m_exitAction, Constants::ID_EXIT);
    cmd->setDefaultKeySequence(QKeySequence(tr("Ctrl+Q")));
    fileMenu->addAction(cmd, Constants::G_FILE_OTHER);
    // connect(m_exitAction, SIGNAL(triggered()), this, SLOT(exit()));

    // Undo Action
    icon = QIcon::fromTheme(QLatin1String("edit-undo"), QIcon(QLatin1String(ICON_UNDO)));
    m_undoAction = new QAction(icon, tr("&Undo"), this);
    cmd = actionManager->registerAction(m_undoAction, Constants::ID_UNDO);
    cmd->setDefaultKeySequence(QKeySequence::Undo);
    cmd->setAttribute(ActionCommand::CA_UpdateText);
    cmd->setDescription(tr("Undo"));
    editMenu->addAction(cmd, Constants::G_EDIT_UNDOREDO);
    toolBar->addAction(cmd, Constants::G_EDIT_UNDOREDO);
    m_undoAction->setEnabled(false);

    // Redo Action
    icon = QIcon::fromTheme(QLatin1String("edit-redo"), QIcon(QLatin1String(ICON_REDO)));
    m_redoAction = new QAction(icon, tr("&Redo"), this);
    cmd = actionManager->registerAction(m_redoAction, Constants::ID_REDO);
    cmd->setDefaultKeySequence(QKeySequence::Redo);
    cmd->setAttribute(ActionCommand::CA_UpdateText);
    cmd->setDescription(tr("Redo"));
    editMenu->addAction(cmd, Constants::G_EDIT_UNDOREDO);
    toolBar->addAction(cmd, Constants::G_EDIT_UNDOREDO);
    m_redoAction->setEnabled(false);

    // Cut Action
    icon = QIcon::fromTheme(QLatin1String("edit-cut"), QIcon(QLatin1String(ICON_CUT)));
    m_cutAction = new QAction(icon, tr("Cu&t"), this);
    cmd = actionManager->registerAction(m_cutAction, Constants::ID_CUT);
    cmd->setDefaultKeySequence(QKeySequence::Cut);
    editMenu->addAction(cmd, Constants::G_EDIT_COPYPASTE);
    toolBar->addAction(cmd, Constants::G_EDIT_COPYPASTE);
    m_cutAction->setEnabled(false);

    // Copy Action
    icon = QIcon::fromTheme(QLatin1String("edit-copy"), QIcon(QLatin1String(ICON_COPY)));
    m_copyAction = new QAction(icon, tr("&Copy"), this);
    cmd = actionManager->registerAction(m_copyAction, Constants::ID_COPY);
    cmd->setDefaultKeySequence(QKeySequence::Copy);
    editMenu->addAction(cmd, Constants::G_EDIT_COPYPASTE);
    toolBar->addAction(cmd, Constants::G_EDIT_COPYPASTE);
    m_copyAction->setEnabled(false);

    // Paste Action
    icon = QIcon::fromTheme(QLatin1String("edit-paste"), QIcon(QLatin1String(ICON_PASTE)));
    m_pasteAction = new QAction(icon, tr("&Paste"), this);
    cmd = actionManager->registerAction(m_pasteAction, Constants::ID_PASTE);
    cmd->setDefaultKeySequence(QKeySequence::Paste);
    editMenu->addAction(cmd, Constants::G_EDIT_COPYPASTE);
    toolBar->addAction(cmd, Constants::G_EDIT_COPYPASTE);
    m_pasteAction->setEnabled(false);

    // Option Action
    icon = QIcon(QLatin1String(ICON_OPTION));
    tmpAction = new QAction(icon, tr("&Option..."), this);
    cmd = actionManager->registerAction(tmpAction, Constants::ID_OPTION);
    toolsMenu->addAction(cmd, Constants::G_TOOLS_OPTION);

    // Help Action
    icon = QIcon(QLatin1String(ICON_HELPCONTENTS));
    tmpAction = new QAction(icon, tr("&Help Contents..."), this);
    cmd = actionManager->registerAction(tmpAction, Constants::ID_HELPCONTENTS);
    cmd->setDefaultKeySequence(QKeySequence::HelpContents);
    helpMenu->addAction(cmd, Constants::G_HELP_HELPCONTENTS);

    // About Action
    icon = QIcon(QLatin1String(ICON_ABOUT));
    tmpAction = new QAction(icon, tr("&About Orbits Writer..."), this);
    cmd = actionManager->registerAction(tmpAction, Constants::ID_ABOUT);
    helpMenu->addAction(cmd, Constants::G_HELP_ABOUT);
}

void MainWindow::addEditArea()
{
    QTabWidget *editorStack = new QTabWidget(this);
    editorStack->setTabPosition(QTabWidget::South);
    connect(editorStack, SIGNAL(currentChanged(int)),
            this, SLOT(editorChanged(int)));

    m_visualEditor = new VisualEditor(editorStack);
    m_visualEditor->setStyleSheet("border: 0");
    editorStack->insertTab(IDX_VISUALEDITOR, m_visualEditor, tr("Visual"));
    // Visual Editor Connections
    FORWARD_ACTION(m_undoAction, QWebPage::Undo);
    FORWARD_ACTION(m_redoAction, QWebPage::Redo);
    FORWARD_ACTION(m_cutAction, QWebPage::Cut);
    FORWARD_ACTION(m_copyAction, QWebPage::Copy);
    FORWARD_ACTION(m_pasteAction, QWebPage::Paste);
    connect(m_visualEditor, SIGNAL(selectionChanged()),
            this, SLOT(adjustActions()));

    m_sourceEditor = new SourceEditor(editorStack);
    m_sourceEditor->setStyleSheet("border: 0");
    editorStack->insertTab(IDX_SOURCEEDITOR, m_sourceEditor, tr("Source"));

    m_titleEdit = new QLineEdit(this);
    m_titleEdit->setFixedHeight(40);
    QFont defaultFont;
    defaultFont.setPointSize(24);
    m_titleEdit->setFont(defaultFont);
    m_titleEdit->setStyleSheet("border:2px solid gray;"
                               "border-radius: 10px;"
                               "padding:0 8px;");

    QWidget *editorArea = new QWidget(this);
    QVBoxLayout *editorAreaLayout = new QVBoxLayout;
    editorAreaLayout->setContentsMargins(4, 6, 4, 0);
    editorAreaLayout->setMargin(4);
    editorAreaLayout->addWidget(m_titleEdit);
    editorAreaLayout->addWidget(editorStack);
    editorArea->setLayout(editorAreaLayout);
    setCentralWidget(editorArea);

    m_visualEditor->setFocus();
}

void MainWindow::adjustActions()
{
    FOLLOW_ENABLE(m_undoAction, QWebPage::Undo);
    FOLLOW_ENABLE(m_redoAction, QWebPage::Redo);
    FOLLOW_ENABLE(m_cutAction, QWebPage::Cut);
    FOLLOW_ENABLE(m_copyAction, QWebPage::Copy);
    FOLLOW_ENABLE(m_pasteAction, QWebPage::Paste);
}

void MainWindow::editorChanged(int index)
{
    DocumentManager *documentManager = appCore->documentManager();
    switch(index) {
    case IDX_VISUALEDITOR:
        // Note that when visual editor add to tab widget first,
        // the m_sourceEditor will be 0. We have to check this condition.
        if (m_sourceEditor) {
            documentManager->writeCurrentDocument(m_sourceEditor->toPlainText());
        }
        break;
    case IDX_SOURCEEDITOR:
        documentManager->writeCurrentDocument(m_visualEditor->toFormattedHtml());
        break;
    }
}


static const char settingsGroup[]     = "MainWindow";
static const char windowGeometryKey[] = "WindowGeometry";
static const char windowStateKey[]    = "WindowState";

void MainWindow::readSettings()
{
    QSettings *settings = appCore->settingsManager()->settings();

    settings->beginGroup(QLatin1String(settingsGroup));
    if (!restoreGeometry(settings->value(QLatin1String(windowGeometryKey)).toByteArray())) {
        resize(1008, 700); // size without window decoration
    }
    restoreState(settings->value(QLatin1String(windowStateKey)).toByteArray());
    settings->endGroup();
}

void MainWindow::writeSettings()
{
    QSettings *settings = appCore->settingsManager()->settings();

    settings->beginGroup(QLatin1String(settingsGroup));
    settings->setValue(QLatin1String(windowGeometryKey), saveGeometry());
    settings->setValue(QLatin1String(windowStateKey), saveState());
    settings->endGroup();
}
