/*-------------------------------------------------
 *
 * OrbitsWriter - an Offline Blog Writer
 *
 * Copyright (C) 2012 devbean@galaxyworld.org
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 *-------------------------------------------------*/

#ifndef DOCUMENT_H
#define DOCUMENT_H

#include <QObject>

namespace Core
{

/*!
  The Document class is the content of the document.

  Document will be the core model of any editors, such as visual
  or source editor. Any changes made by editors will effect this model.

  Document manages by DocumentManager.

  \sa DocumentManager
 */
class Document : public QObject
{
    Q_OBJECT
public:
    /*!
      Constructs an empty document with given \a parent.
     */
    explicit Document(QObject *parent = 0);
    
signals:
    /*!
      Emits when contents of this document changed.

      The new content will be stored in \a content.
     */
    void contentChanged(const QString &content);

public slots:
    /*!
      Initializes the document content.

      This function will clear all contents and flags within
      the document.
     */
    void init();

    /*!
      Sets content to \a content
     */
    void setContent(const QString &content);
    
private:
    QString m_content;
};

} // end of namespace Core

#endif // DOCUMENT_H
