/*-------------------------------------------------
 *
 * OrbitsWriter - an Offline Blog Writer
 *
 * Copyright (C) 2012 devbean@galaxyworld.org
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 *-------------------------------------------------*/

#include <QActionGroup>
#include <QApplication>
#include <QDebug>
#include <QFontComboBox>
#include <QtPlugin>
#include <QStringList>
#include <QMenu>
#include <QWidgetAction>

#include <common/colortoolbutton.h>
#include <core/actionsystem/actioncontainer.h>
#include <core/appcore.h>
#include <core/coreconstants.h>
#include <core/editors/visualeditor.h>
#include "textformattingplugin.h"

using namespace Core;

namespace TextFormatting
{
namespace Internal
{

/*!
  Action for font combo box widget.
 */
class FontComboBoxAction : public QWidgetAction
{
public:
    FontComboBoxAction(QObject *parent = 0) :
        QWidgetAction(parent)
    {
    }

protected:
    QWidget *createWidget(QWidget *parent)
    {
        return new QFontComboBox(parent);
    }
}; // end of class TextFormatting::Internal::FontComboBoxAction


/*!
  Action for text color widget.
 */
class TextColorButtonAction : public QWidgetAction
{
public:
    TextColorButtonAction(const char icon[], const QColor &color, QObject *parent = 0) :
        QWidgetAction(parent),
        icon(QIcon(QLatin1String(icon))),
        defaultColor(color)
    {
    }

    QIcon icon;
    QColor defaultColor;

protected:
    QWidget *createWidget(QWidget *parent)
    {
        Common::ColorToolButton *button = new Common::ColorToolButton(parent);
        button->setIcon(icon);
        button->setCurrentColor(defaultColor);
        return button;
    }

}; // end of class TextFormatting::Internal::FontComboBoxAction

} // end of namespace TextFormatting::Internal

using namespace TextFormatting::Internal;

// Icons
//! Path for font icon.
static const char ICON_TEXTFONT[]                   = ":/images/font";
//! Path for text color icon.
static const char ICON_TEXTCOLOR[]                  = ":/images/text_color";
//! Path for text color icon on tool bar.
static const char ICON_TOOLBARTEXTCOLOR[]           = ":/images/tb_text_color_18x18";
//! Path for text background color icon.
static const char ICON_TEXTBACKGROUNDCOLOR[]        = ":/images/text_background_color";
//! Path for text background color icon on tool bar.
static const char ICON_TOOLBARTEXTBACKGROUNDCOLOR[] = ":/images/tb_text_background_color_18x18";
//! Path for alignment center icon.
static const char ICON_ALIGNCENTER[]                = ":/images/align_center";
//! Path for alignment left icon.
static const char ICON_ALIGNLEFT[]                  = ":/images/align_left";
//! Path for alignment right icon.
static const char ICON_ALIGNRIGHT[]                 = ":/images/align_right";
//! Path for alignment fill icon.
static const char ICON_ALIGNFILL[]                  = ":/images/align_fill";
//! Path for text bold icon.
static const char ICON_TEXTBOLD[]                   = ":/images/text_bold";
//! Path for text italic icon.
static const char ICON_TEXTITALIC[]                 = ":/images/text_italic";
//! Path for text strike icon.
static const char ICON_TEXTSTRIKE[]                 = ":/images/text_strike";
//! Path for text underline icon.
static const char ICON_TEXTUNDERLINE[]              = ":/images/text_underline";

// Action Unique Identifiers
//! Unique identifier name for text font action.
static const char ID_FORMAT_TEXTFONT[]                   = "OrbitsWriter.TextFormat.TextFont";
//! Unique identifier name for text font action on tool bar.
static const char ID_FORMAT_TOOLBARTEXTFONT[]            = "OrbitsWriter.TextFormat.ToolBarTextFont";
//! Unique identifier name for text color action.
static const char ID_FORMAT_TEXTCOLOR[]                  = "OrbitsWriter.TextFormat.TextColor";
//! Unique identifier name for text color action on tool bar.
static const char ID_FORMAT_TOOLBARTEXTCOLOR[]           = "OrbitsWriter.TextFormat.ToolBarTextColor";
//! Unique identifier name for text background color action.
static const char ID_FORMAT_TEXTBACKGROUNDCOLOR[]        = "OrbitsWriter.TextFormat.TextBackgroundColor";
//! Unique identifier name for text background color action on tool bar.
static const char ID_FORMAT_TOOLBARTEXTBACKGROUNDCOLOR[] = "OrbitsWriter.TextFormat.ToolBarTextBackgroundColor";
//! Unique identifier name for alignment center action.
static const char ID_FORMAT_ALIGNCENTER[]                = "OrbitsWriter.TextFormat.AlignCenter";
//! Unique identifier name for alignment left action.
static const char ID_FORMAT_ALIGNLEFT[]                  = "OrbitsWriter.TextFormat.AlignLeft";
//! Unique identifier name for alignment right action.
static const char ID_FORMAT_ALIGNRIGHT[]                 = "OrbitsWriter.TextFormat.AlignRight";
//! Unique identifier name for alignment fill action.
static const char ID_FORMAT_ALIGNFILL[]                  = "OrbitsWriter.TextFormat.AlignFill";
//! Unique identifier name for text bold action.
static const char ID_FORMAT_TEXTBOLD[]                   = "OrbitsWriter.TextFormat.Bold";
//! Unique identifier name for text italic action.
static const char ID_FORMAT_TEXTITALIC[]                 = "OrbitsWriter.TextFormat.Italic";
//! Unique identifier name for text strike action.
static const char ID_FORMAT_TEXTSTRIKE[]                 = "OrbitsWriter.TextFormat.Strike";
//! Unique identifier name for text underline action.
static const char ID_FORMAT_TEXTUNDERLINE[]              = "OrbitsWriter.TextFormat.Underline";

TextFormattingPlugin::TextFormattingPlugin() :
    m_textBoldAction(0),
    m_textItalicAction(0),
    m_textStrikeAction(0),
    m_textUnderlineAction(0),
    m_alignLeftAction(0),
    m_alignCenterAction(0),
    m_alignRightAction(0),
    m_alignFillAction(0),
    m_visualEditor(0)
{
}

TextFormattingPlugin::~TextFormattingPlugin()
{
}

bool TextFormattingPlugin::initialize(const QStringList &arguments, QString *errorString)
{
    Q_UNUSED(arguments);
    Q_UNUSED(errorString);

    ActionManager   *actionManager = appCore->actionManager();
    ActionContainer *formatMenu = actionManager->actionContainer(Constants::M_FORMAT);
    ActionContainer *toolBar = actionManager->actionContainer(Constants::DEFAULT_TOOL_BAR);

    // Text Bold Action
    QIcon icon = QIcon(QLatin1String(ICON_TEXTBOLD));
    m_textBoldAction = new QAction(icon, tr("&Bold"), this);
    m_textBoldAction->setCheckable(true);
    m_textBoldAction->setShortcut(Qt::CTRL + Qt::Key_B);
    ActionCommand *cmd = actionManager->registerAction(m_textBoldAction, ID_FORMAT_TEXTBOLD);
    formatMenu->addAction(cmd, Constants::G_FORMAT_TEXTSTYLE);
    toolBar->addAction(cmd, Constants::G_FORMAT_TEXTSTYLE);

    // Text Italic Action
    icon = QIcon(QLatin1String(ICON_TEXTITALIC));
    m_textItalicAction = new QAction(icon, tr("&Italic"), this);
    m_textItalicAction->setShortcut(Qt::CTRL + Qt::Key_I);
    m_textItalicAction->setCheckable(true);
    cmd = actionManager->registerAction(m_textItalicAction, ID_FORMAT_TEXTITALIC);
    formatMenu->addAction(cmd, Constants::G_FORMAT_TEXTSTYLE);
    toolBar->addAction(cmd, Constants::G_FORMAT_TEXTSTYLE);

    // Text Strike Action
    icon = QIcon(QLatin1String(ICON_TEXTSTRIKE));
    m_textStrikeAction = new QAction(icon, tr("Strike"), this);
    m_textStrikeAction->setShortcut(Qt::CTRL + Qt::Key_D);
    m_textStrikeAction->setCheckable(true);
    cmd = actionManager->registerAction(m_textStrikeAction, ID_FORMAT_TEXTSTRIKE);
    formatMenu->addAction(cmd, Constants::G_FORMAT_TEXTSTYLE);
    toolBar->addAction(cmd, Constants::G_FORMAT_TEXTSTYLE);

    // Text Underline Action
    icon = QIcon(QLatin1String(ICON_TEXTUNDERLINE));
    m_textUnderlineAction = new QAction(icon, tr("&Underline"), this);
    m_textUnderlineAction->setShortcut(Qt::CTRL + Qt::Key_U);
    m_textUnderlineAction->setCheckable(true);
    cmd = actionManager->registerAction(m_textUnderlineAction, ID_FORMAT_TEXTUNDERLINE);
    formatMenu->addAction(cmd, Constants::G_FORMAT_TEXTSTYLE);
    toolBar->addAction(cmd, Constants::G_FORMAT_TEXTSTYLE);

    // Text Font Action
    icon = QIcon(QLatin1String(ICON_TEXTFONT));
    QAction *textFontAction = new QAction(icon, tr("&Font..."), this);
    cmd = actionManager->registerAction(textFontAction, ID_FORMAT_TEXTFONT);
    formatMenu->addAction(cmd, Constants::G_FORMAT_FONTCOLOR);

    // Tool Bar Text Font Action
    FontComboBoxAction *toolBarTextFontAction = new FontComboBoxAction(this);
    cmd = actionManager->registerAction(toolBarTextFontAction, ID_FORMAT_TOOLBARTEXTFONT);
    toolBar->addAction(cmd, Constants::G_FORMAT_FONTCOLOR);

    // Text Color Action
    icon = QIcon(QLatin1String(ICON_TEXTCOLOR));
    QAction *textColorAction = new QAction(icon, tr("Text &Color..."), this);
    cmd = actionManager->registerAction(textColorAction, ID_FORMAT_TEXTCOLOR);
    formatMenu->addAction(cmd, Constants::G_FORMAT_FONTCOLOR);

    // Tool Bar Text Color Action
    TextColorButtonAction *toolBarTextColorAction = new TextColorButtonAction(ICON_TOOLBARTEXTCOLOR, Qt::black, this);
    cmd = actionManager->registerAction(toolBarTextColorAction, ID_FORMAT_TOOLBARTEXTCOLOR);
    toolBar->addAction(cmd, Constants::G_FORMAT_FONTCOLOR);

    // Text Background Color Action
    icon = QIcon(QLatin1String(ICON_TEXTBACKGROUNDCOLOR));
    QAction *textBackgroundColorAction = new QAction(icon, tr("Text &Background Color..."), this);
    cmd = actionManager->registerAction(textBackgroundColorAction, ID_FORMAT_TEXTBACKGROUNDCOLOR);
    formatMenu->addAction(cmd, Constants::G_FORMAT_FONTCOLOR);

    // Tool Bar Text Background Color Action
    TextColorButtonAction *toolBarTextBackgroundColorAction = new TextColorButtonAction(ICON_TOOLBARTEXTBACKGROUNDCOLOR, Qt::white, this);
    cmd = actionManager->registerAction(toolBarTextBackgroundColorAction, ID_FORMAT_TOOLBARTEXTBACKGROUNDCOLOR);
    toolBar->addAction(cmd, Constants::G_FORMAT_FONTCOLOR);

    // Alignment Left Action
    icon = QIcon(QLatin1String(ICON_ALIGNLEFT));
    m_alignLeftAction = new QAction(icon, tr("Align Left"), this);
    m_alignLeftAction->setShortcut(Qt::CTRL + Qt::Key_L);
    ActionCommand *cmdAlignLeft = actionManager->registerAction(m_alignLeftAction, ID_FORMAT_ALIGNLEFT);

    // Alignment Center Action
    icon = QIcon(QLatin1String(ICON_ALIGNCENTER));
    m_alignCenterAction = new QAction(icon, tr("Align Center"), this);
    m_alignCenterAction->setShortcut(Qt::CTRL + Qt::Key_E);
    ActionCommand *cmdAlignCenter = actionManager->registerAction(m_alignCenterAction, ID_FORMAT_ALIGNCENTER);

    // Alignment Right Action
    icon = QIcon(QLatin1String(ICON_ALIGNRIGHT));
    m_alignRightAction = new QAction(icon, tr("Align Right"), this);
    m_alignRightAction->setShortcut(Qt::CTRL + Qt::Key_R);
    ActionCommand *cmdAlignRight = actionManager->registerAction(m_alignRightAction, ID_FORMAT_ALIGNRIGHT);

    if (QApplication::isLeftToRight()) {
        formatMenu->addAction(cmdAlignLeft, Constants::G_FORMAT_ALIGNMENT);
        toolBar->addAction(cmdAlignLeft, Constants::G_FORMAT_ALIGNMENT);
        formatMenu->addAction(cmdAlignCenter, Constants::G_FORMAT_ALIGNMENT);
        toolBar->addAction(cmdAlignCenter, Constants::G_FORMAT_ALIGNMENT);
        formatMenu->addAction(cmdAlignRight, Constants::G_FORMAT_ALIGNMENT);
        toolBar->addAction(cmdAlignRight, Constants::G_FORMAT_ALIGNMENT);
    } else {
        formatMenu->addAction(cmdAlignRight, Constants::G_FORMAT_ALIGNMENT);
        toolBar->addAction(cmdAlignRight, Constants::G_FORMAT_ALIGNMENT);
        formatMenu->addAction(cmdAlignCenter, Constants::G_FORMAT_ALIGNMENT);
        toolBar->addAction(cmdAlignCenter, Constants::G_FORMAT_ALIGNMENT);
        formatMenu->addAction(cmdAlignLeft, Constants::G_FORMAT_ALIGNMENT);
        toolBar->addAction(cmdAlignLeft, Constants::G_FORMAT_ALIGNMENT);
    }

    // Alignment Fill Action
    icon = QIcon(QLatin1String(ICON_ALIGNFILL));
    m_alignFillAction = new QAction(icon, tr("Align Fill"), this);
    m_alignFillAction->setShortcut(Qt::CTRL + Qt::Key_F);
    cmd = actionManager->registerAction(m_alignFillAction, ID_FORMAT_ALIGNFILL);
    formatMenu->addAction(cmd, Constants::G_FORMAT_ALIGNMENT);
    toolBar->addAction(cmd, Constants::G_FORMAT_ALIGNMENT);

    return true;
}

#define FORWARD_ACTION(a1, a2) \
    connect(a1, SIGNAL(triggered()), \
            m_visualEditor->pageAction(a2), SLOT(trigger())); \
    connect(m_visualEditor->pageAction(a2), SIGNAL(changed()), \
            this, SLOT(adjustActions()));

void TextFormattingPlugin::dependenciesInitialized()
{
    m_visualEditor = appCore->objectPool()->getObject<VisualEditor>();

    connect(m_visualEditor->page(), SIGNAL(selectionChanged()), SLOT(adjustActions()));

    FORWARD_ACTION(m_textBoldAction, QWebPage::ToggleBold);
    FORWARD_ACTION(m_textItalicAction, QWebPage::ToggleItalic);
    FORWARD_ACTION(m_textUnderlineAction, QWebPage::ToggleUnderline);
    FORWARD_ACTION(m_textStrikeAction, QWebPage::ToggleStrikethrough);
    FORWARD_ACTION(m_alignCenterAction, QWebPage::AlignCenter);
    FORWARD_ACTION(m_alignFillAction, QWebPage::AlignJustified);
    FORWARD_ACTION(m_alignLeftAction, QWebPage::AlignLeft);
    FORWARD_ACTION(m_alignRightAction, QWebPage::AlignRight);

    adjustActions();
}

#define FOLLOW_CHECK(a1, a2) a1->setChecked(m_visualEditor->pageAction(a2)->isChecked())

void TextFormattingPlugin::adjustActions()
{
    FOLLOW_CHECK(m_textBoldAction, QWebPage::ToggleBold);
    FOLLOW_CHECK(m_textItalicAction, QWebPage::ToggleItalic);
    FOLLOW_CHECK(m_textUnderlineAction, QWebPage::ToggleUnderline);
    FOLLOW_CHECK(m_textStrikeAction, QWebPage::ToggleStrikethrough);
}

} // end of namespace TextFormatting

Q_EXPORT_PLUGIN2(TextFormattingPlugin, TextFormatting::TextFormattingPlugin)
