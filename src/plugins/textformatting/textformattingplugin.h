/*-------------------------------------------------
 *
 * OrbitsWriter - an Offline Blog Writer
 *
 * Copyright (C) 2012 devbean@galaxyworld.org
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 *-------------------------------------------------*/

#ifndef TEXTFORMATTINGPLUGIN_H
#define TEXTFORMATTINGPLUGIN_H

#include <pluginsystem/plugin.h>

class QAction;

namespace Core
{
class VisualEditor;
} // end of namespace Core

namespace TextFormatting
{

class TextFormattingPlugin : public PluginSystem::Plugin
{
    Q_OBJECT
    Q_PLUGIN_METADATA(IID "org.galaxyworld.orbitswriter.plugin" FILE "TextFormatting.json")
public:
    TextFormattingPlugin();
    ~TextFormattingPlugin();

    bool initialize(const QStringList &arguments, QString *errorString);
    void dependenciesInitialized();

private slots:
    void adjustActions();

private:
    QAction *m_textBoldAction;
    QAction *m_textItalicAction;
    QAction *m_textStrikeAction;
    QAction *m_textUnderlineAction;
    QAction *m_alignLeftAction;
    QAction *m_alignCenterAction;
    QAction *m_alignRightAction;
    QAction *m_alignFillAction;

    Core::VisualEditor *m_visualEditor;
}; // end of class TextFormattingPlugin::TextFormattingPlugin

} // end of namespace TextFormattingPlugin

#endif // TEXTFORMATTINGPLUGIN_H
