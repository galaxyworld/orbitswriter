# USE .subdir AND .depends !
# OTHERWISE PLUGINS WILL BUILD IN WRONG ORDER (DIRECTORIES ARE COMPILED IN PARALLEL)

TEMPLATE  = subdirs

SUBDIRS   = \
    plugin_core \
    plugin_textformatting

include(../../orbitswriter.pri)

plugin_core.subdir = core

plugin_textformatting.subdir = textformatting
plugin_textformatting.depends = plugin_core
